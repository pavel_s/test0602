<?php

namespace App\Models\T01;

use App\Models\Import\ImportLot;
use App\Models\Import\TorgiGov\Lot as TorgiGovLot;
use App\Models\Import\EFRSB\Lot as EfrsbLot;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Lot
 * @package App\Models\T01
 * @property int $id
 * @property string $src_code
 * @property int $src_id
 * @property string $src_num
 * @property string $publish_date
 * @property string $property_type
 * @property string $property_name
 * @property string $property_address
 * @property string $property_cn
 * @property string $property_info
 * @property string $kladr_code
 * @property string $debtor_type
 * @property string $debtor_name
 * @property string $bid_date
 * @property string $bid_status_id
 * @property string $bid_type
 * @property string $bid_place
 * @property string $price_start
 * @property string $price_final
 * @property string $price_currency
 * @method static Lot find(int $id)
 * @method static Lot firstOrCreate(array $data)
 */
class Lot extends Model
{
    protected $table = 't01_lot';
    protected $guarded = [];

    const DEBTOR_TYPE_IND = 'ind';
    const DEBTOR_TYPE_ORG = 'org';
    const DEBTOR_TYPE_GOV = 'gov';

    /**
     * @param ImportLot $importLot
     * @return Lot
     */
    public static function convertFromImportLot(ImportLot $importLot): self
    {
        $lot = Lot::firstOrCreate([
            'src_code' => $importLot->getSrcCode(),
            'src_id' => $importLot->getSrcId(),
        ]);

        $lot->src_num = $importLot->getSrcNum();
        $lot->publish_date = $importLot->getPublishDate();
        $lot->property_type = $importLot->getPropertyTypeId();
        $lot->property_name = $importLot->getPropertyName();
        $lot->property_address = $importLot->getPropertyAddress();
        $lot->property_cn = $importLot->getPropertyCN();
        $lot->property_info = json_encode($importLot->getPropertyInfo());
        $lot->kladr_code = $importLot->getKladrCode();
        $lot->debtor_type = $importLot->getDebtorType();
        $lot->debtor_name = $importLot->getDebtorName();
        $lot->bid_date = $importLot->getBidDate();
        $lot->bid_status_id = $importLot->getBidStatusId();
        $lot->bid_type = $importLot->getBidType();
        $lot->bid_place = $importLot->getBidPlace();
        $lot->price_start = $importLot->getStartPrice();
        $lot->price_final = $importLot->getFinalPrice();
        $lot->price_currency = $importLot->getCurrencyCode();
        $lot->save();

        return $lot;
    }

    public function getSrcLot(): ?ImportLot
    {
        return self::getImportLot($this->src_code, $this->src_id);
    }

    public static function getImportLot($srcCode, $srcId): ?ImportLot
    {
        if ($srcCode == TorgiGovLot::srcCode) {
            return TorgiGovLot::find($srcId);
        }
        if ($srcCode == EfrsbLot::srcCode) {
            return EfrsbLot::find($srcId);
        }
        return null;
    }


}
