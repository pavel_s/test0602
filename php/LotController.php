<?php

namespace App\Http\Controllers\Bid;

use App\Models\T01\Lot;
use App\Models\T01\LotUser;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class LotController extends BaseController
{

    public function search(Request $request)
    {
        $cond = [
            't01_lot.kladr_code like :kladr',
            't01_lot.publish_date between :date_from and :date_to'
        ];
        $params = [
            'kladr' => $request->get('kladr', '00') . '%',
            'date_from' => $request->get('date_from') . ' 00:00:00',
            'date_to' => $request->get('date_to') . ' 23:59:59',
            'user_id' => Auth::guard('api')->user() ? Auth::guard('api')->user()->id : null,
        ];
        if ($request->get('property_types')) {
            $cond[] = 'FIND_IN_SET(t01_lot.property_type, :property_types)';
            $params['property_types'] = implode(',', (array)$request->get('property_types'));
        }
        if ($request->get('bid_statuses')) {
            $cond[] = 'FIND_IN_SET(t01_lot.bid_status_id, :bid_statuses)';
            $params['bid_statuses'] = implode(',', (array)$request->get('bid_statuses'));
        }
        if ($request->get('hide')) {
            $cond[] = 't01_lot_user.hide = 1';
        } else {
            $cond[] = 'coalesce(t01_lot_user.hide, 0) = 0';
        }
        if ($request->get('favorite')) {
            $cond[] = 't01_lot_user.favorite = 1';
        }


        $query = "select t01_lot.id,
            t01_lot_status.id as bid_status_id,
            t01_lot_status.name as bid_status_title,
            t01_lot_status.map_icon as bid_status_map_icon,
            pt.id as property_type_id,
            concat(coalesce(concat(ppt.name, ', '), ''), pt.name) as property_type_name,
            t01_lot.src_code,
            t01_lot.src_id,
            t01_lot.src_num,
            t01_lot.publish_date,
            t01_lot.property_name,
            t01_lot.property_address,
            t01_lot.property_cn,
            t01_lot.debtor_type,
            t01_lot.debtor_name,
            t01_lot.price_start,
            t01_lot.price_final,
            t01_lot.price_currency,
            t01_lot_user.hide as user_hide,
            t01_lot_user.favorite as user_favorite,
            t01_lot_user.note as user_note,
            cadastr.ya_kind,
            cadastr.ya_address,
            cadastr.ya_lat,
            cadastr.ya_lng
            from t01_lot
            left join t01_lot_status on t01_lot.bid_status_id = t01_lot_status.id
            left join t01_property_type pt on pt.id = t01_lot.property_type
            left join t01_property_type ppt on ppt.id = pt.pid
            left join cadastr on cadastr.num = t01_lot.property_cn
            left join t01_lot_user on t01_lot_user.lot_id = t01_lot.id and t01_lot_user.user_id = :user_id
            where " . implode(' and ', $cond) . "
            order by t01_lot.publish_date desc
            limit 300";
        $currencies = ['RUB' => '₽', 'USD' => '$', 'EUR' => '€'];

        return array_map(function ($r) use ($currencies) {
            return [
                'id' => $r->id,
                'src_code' => $r->src_code,
                'src_id' => $r->src_id,
                'src_num' => $r->src_num,
                'publish_date' => $r->publish_date,
                'status' => [
                    'id' => $r->bid_status_id,
                    'title' => $r->bid_status_title,
                    'map_icon' => $r->bid_status_map_icon,
                ],
                'property' => [
                    'type_id' => $r->property_type_id,
                    'type_name' => $r->property_type_name,
                    'name' => $r->property_name,
                    'address' => $r->property_address,
                    'cn' => $r->property_cn,
                    'ya_kind' => $r->ya_kind,
                    'ya_address' => $r->ya_address,
                    'ya_lat' => $r->ya_lat,
                    'ya_lng' => $r->ya_lng,
                ],
                'debtor' => [
                    'type' => $r->debtor_type,
                    'name' => $r->debtor_name,
                ],
                'price' => [
                    'start' => $r->price_start,
                    'final' => $r->price_final,
                    'currency' => $currencies[$r->price_currency] ?? '',
                ],
                'user' => Auth::guard('api')->user() ? [
                    'hide' => (bool)$r->user_hide,
                    'favorite' => (bool)$r->user_favorite,
                    'note' => $r->user_note,
                ] : null,
                'showComment' => false,
            ];
        }, DB::select($query, $params));
    }

    public function show($id)
    {
        $lot = Lot::find($id);
        if ($lot) {
            $srcLot = $lot->getSrcLot();
            if ($srcLot) {
                return Redirect::to($srcLot->getSrcUrl());
            }
        }
        abort(404);
    }

    public function user(Request $request)
    {
        if (Auth::guard('api')->user()) {
            $lotUser = LotUser::firstOrNew([
                'lot_id' => $request->get('lot_id'),
                'user_id' => Auth::guard('api')->user()->id,
            ]);
            $lotUser->hide = (bool)$request->get('data')['hide'] ?? false;
            $lotUser->favorite = (bool)$request->get('data')['favorite'] ?? false;
            $lotUser->note = $request->get('data')['note'] ?? null;
            $lotUser->save();
            return $lotUser;
        }
    }

}
