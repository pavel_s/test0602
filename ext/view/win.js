Ext.define('app.view.trip.main.win', {
    extend: 'Ext.window.Window',
    alias: 'widget.trip.main.win',
    title: 'Рейс',
    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    width: 1250,
    height: 600,
    minWidth: 900,
    minHeight: 400,
    minimizable: true,
    items: [
        {
            xtype: 'trip.main.form',
            height: 200,
            padding: 0,
            border: 0
        },
        {
            xtype: 'tabpanel',
            flex: 1,
            plain: true,
            padding: 0,
            border: 0,
            style: 'border-top: 1px solid #99bce8',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    title: 'ОСНОВНОЕ',
                    border: 0,
                    padding: 0,
                    flex: 1,
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'trip.main.grid.points_for_tipper',
                            itemId: 'points_for_tipper',
                            hidden: true,
                            flex: 1,
                            border: 0,
                            padding: 0
                        },
                        {
                            xtype: 'trip.main.grid.points_for_container',
                            itemId: 'points_for_container',
                            hidden: true,
                            flex: 1,
                            border: 0,
                            padding: 0
                        }
                    ]
                },
                {
                    title: 'Удаленные',
                    border: 0,
                    padding: 0,
                    flex: 1,
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'trip.main.grid.removed',
                            itemId: 'removed_points',
                            flex: 1,
                            border: 0,
                            padding: 0
                        }
                    ]
                }
            ]
        }
    ],
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'bottom',
            items: [
                {
                    text: 'Отменить',
                    itemId: 'btn_change_state_created',
                    iconCls: 'icon_cross',
                    padding: 5,
                    handler: function () {
                        var win = this.up('window');
                        var form = win.down('form').getForm();
                        var rTrip = form.getRecord();
                        Ext.Msg.prompt('Отказ', 'Укажите причину отказа:', function (btn, text) {
                            if (btn == 'ok' && text != '') {
                                rTrip.set('state_note', text);
                                var cTrip = Ext.create('app.controller.trip.main');
                                cTrip.save(win, 'created');
                            }
                        });
                    }
                },
                {
                    text: 'Старт',
                    itemId: 'btn_change_state_performed',
                    iconCls: 'icon_flag_1',
                    padding: 5,
                    handler: function () {
                        var win = this.up('window');
                        var rTrip = win.down('form').getRecord();
                        var gridPoints = rTrip.get('vehicle_model_type') == 'container' ? win.query('#points_for_container')[0] : win.query('#points_for_tipper')[0];

                        // проверка не выполненной разгрузки в предыдущем рейсе
                        Ext.Ajax.request({
                            url: '/trip/check_unloading_last_trip/' + rTrip.get('id'),
                            success: function (result) {
                                var data = JSON.parse(result.responseText);
                                if ('trip_id' in data) {
                                    // подтверждение на перемещение точки
                                    Ext.Msg.show({
                                        msg: '<div style="line-height: 1.5em; font-size: 14px;">' +
                                        'Внимание!<br>' +
                                        'Машина ' + data.vehicle_title + ' начинает работу <b>загруженной</b>?<br>' +
                                        'Не было разгрузки в рейсе ' + data.trip_id + '' +
                                        '</div>',
                                        buttons: Ext.Msg.YESNOCANCEL,
                                        fn: function (btn) {
                                            if (btn == 'yes') {
                                                var cTrip = Ext.create('app.controller.trip.main');
                                                cTrip.save(win, 'performed_add_trip_point_' + data.trip_point_id);
                                            }
                                            if (btn == 'no') {
                                                var cTrip = Ext.create('app.controller.trip.main');
                                                cTrip.save(win, 'performed');
                                            }
                                        }
                                    });
                                } else {
                                    // подтверждение на старт рейса
                                    Ext.Ajax.request({
                                        url: '/trip/check_tariff_driver/' + rTrip.get('id'),
                                        success: function (result) {
                                            var data = JSON.parse(result.responseText);
                                            if (data.success) {
                                                Ext.Msg.show({
                                                    msg: 'Подтверждаете начало рейса для ТС ' + rTrip.get('vehicle_title') + '?',
                                                    buttons: Ext.Msg.YESNO,
                                                    fn: function (btn) {
                                                        if (btn == 'yes') {
                                                            var cTrip = Ext.create('app.controller.trip.main');
                                                            cTrip.save(win, 'performed');
                                                        }
                                                    }
                                                });
                                            } else {
                                                Ext.MessageBox.alert('Ошибка!', data.errors.join('<br>') + '<br><br>Обратитесь к руководителю!');
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                },
                {
                    text: 'В ремонте',
                    itemId: 'btn_change_state_in_repair',
                    iconCls: 'icon_wrench_orange',
                    padding: 5,
                    handler: function () {
                        var win = this.up('window');
                        var cTrip = Ext.create('app.controller.trip.main');
                        cTrip.save(win, 'in_repair');
                    }
                },
                {
                    text: 'Завершить',
                    itemId: 'btn_change_state_completed',
                    iconCls: 'icon_flag_finish',
                    padding: 5,
                    handler: function () {
                        var win = this.up('window');
                        var grid = win.down('grid');
                        var cnt_empty = 0;
                        var is_not_start = false; // не начатые пути
                        var is_not_finish = false; // не законченные пути
                        grid.store.each(function (rTripPoint) {
                            if (!rTripPoint.get('volume_fact') && !rTripPoint.get('volume_fict')) {
                                if (rTripPoint.get('type') == 'loading') {
                                    is_not_start = true;
                                }
                                if (rTripPoint.get('type') == 'unloading') {
                                    is_not_finish = true;
                                }
                            }
                        });
                        Ext.Msg.show({
                            msg: (is_not_start || is_not_finish ? 'ВНИМАНИЕ! В Рейсе есть строки с незаполненными данными загрузки/разгрузки!<br>' : '') + 'Перевести рейс в статус «Завершен»?',
                            buttons: Ext.Msg.YESNO,
                            fn: function (btn) {
                                if (btn == 'yes') {
                                    var cTrip = Ext.create('app.controller.trip.main');
                                    cTrip.save(win, 'completed');
                                }
                            }
                        });
                    }
                },
                {
                    text: 'Проверено',
                    itemId: 'btn_change_state_checked',
                    iconCls: 'icon_thumb_up',
                    padding: 5,
                    handler: function () {
                        var win = this.up('window');
                        var grid = win.down('grid');
                        var rTrip = win.down('form').getRecord();
                        var cTrip = Ext.create('app.controller.trip.main');

                        // проверка на наличие доп. соглашения по аренде
                        Ext.Ajax.request({
                            url: '/trip/check_contract_rent/' + rTrip.get('id'),
                            success: function (result) {
                                var data = JSON.parse(result.responseText);
                                if (data.success) {
                                    Ext.Msg.show({
                                        msg: ('Вы уверены в правильности данных?'),
                                        buttons: Ext.Msg.YESNO,
                                        fn: function (btn) {
                                            if (btn == 'yes') {
                                                // проверка на объем
                                                if (rTrip.get('vehicle_model_type') == 'tipper') {
                                                    var warning = [];
                                                    grid.store.each(function (rTripPoint) {
                                                        var volume = rTripPoint.get('volume_fict') ? rTripPoint.get('volume_fict') : rTripPoint.get('volume_fact');
                                                        if (volume < rTrip.get('max_volume') && (
                                                            (rTripPoint.get('nomenclature_delivery_type') == 'supplier_client' && rTripPoint.get('type') == 'unloading') ||
                                                            (rTripPoint.get('nomenclature_delivery_type') == 'client_supplier' && rTripPoint.get('type') == 'loading') ||
                                                            (rTripPoint.get('nomenclature_delivery_type') == 'client_client' && rTripPoint.get('type') == 'loading'))) {
                                                            warning.push('Для Клиента ' + rTripPoint.get('contractor_title') + ' объем выполнения меньше объема ТС (точка ' + rTripPoint.get('id') + ')!');
                                                        }
                                                    });
                                                    if (warning.length) {
                                                        Ext.Msg.show({
                                                            msg: 'Внимание!<br><br>' + warning.join('<br>') + '<br><br>Продолжить?',
                                                            buttons: Ext.Msg.YESNO,
                                                            fn: function (btn) {
                                                                if (btn == 'yes') {
                                                                    cTrip.save(win, 'checked');
                                                                }
                                                            }
                                                        });
                                                    } else {
                                                        cTrip.save(win, 'checked');
                                                    }
                                                } else {
                                                    cTrip.save(win, 'checked');
                                                }
                                            }
                                        }
                                    });
                                } else {
                                    Ext.MessageBox.alert('Ошибка!', data.errors.join('<br>') + '<br><br>Перевод в статус "проверено" невозможен!<br>Обратитесь к менеджеру по продажам!');
                                }
                            }
                        });
                    }
                },
                '->',
                {
                    text: 'Печать',
                    itemId: 'print',
                    iconCls: 'icon_printer',
                    padding: 5,
                    menu: [
                        {
                            text: 'Маршрутный лист',
                            handler: function () {
                                var rTrip = this.up('window').down('form').getRecord();
                                window.open('/trip/print/list/' + rTrip.get('id'), '_blank');
                            }
                        },
                        {
                            text: 'Справка ЭСМ-7',
                            itemId: 'btn_print_esm7',
                            handler: function () {
                                var rTrip = this.up('window').down('form').getRecord();
                                var win = Ext.create('app.view.trip.list_client.win');
                                var grid = win.down('grid');

                                grid.store.proxy.extraParams = {
                                    contractor_debt_trip_id: rTrip.get('id'),
                                    contractor_debt_contractor_type: 'client'
                                };

                                grid.store.load(function (records, operation, success) {
                                    if (success) {
                                        if (records.length == 1) {
                                            window.open('/report/register_contractor_nomenclature/print_esm7?trip_id=' + rTrip.get('id'), '_blank');
                                        } else if (records.length > 1) {
                                            win.show();
                                            win.query('#btn-select')[0].on('click', function () {
                                                var contractors = [];
                                                grid.store.each(function (rContractor) {
                                                    if (rContractor.get('use')) {
                                                        contractors.push(rContractor.get('id'));
                                                    }
                                                });
                                                if (contractors.length > 0) {
                                                    var params = {
                                                        trip_id: rTrip.get('id'),
                                                        contractors: contractors.join(',')
                                                    };
                                                    window.open('/report/register_contractor_nomenclature/print_esm7?' + Ext.Object.toQueryString(params), '_blank');
                                                }
                                                win.close();
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    ]
                },

                {
                    text: 'Сохранить',
                    itemId: 'btn_save',
                    iconCls: 'icon_disk',
                    padding: 5,
                    handler: function () {
                        var win = this.up('window');
                        var cTrip = Ext.create('app.controller.trip.main');
                        cTrip.save(win, null);
                    }
                },
                {
                    text: 'Выход',
                    iconCls: 'icon_door_in',
                    padding: 5,
                    handler: function () {
                        this.up('window').close();
                    }
                }
            ]
        }
    ],
    listeners: {
        minimize: function () {
            App.Global.Collapse.collapseWidth(this);
        },
        close: function () {
            App.Global.Collapse.collapseWidthClose(this);
        }
    }
});