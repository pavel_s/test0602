Ext.define('app.view.trip.main.form', {
    extend: 'Ext.form.FormPanel',
    alias: 'widget.trip.main.form',
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    items: [
        {
            xtype: 'fieldset',
            title: 'План',
            padding: '2 10',
            margin: '5 10 10 10',
            flex: 5,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'datefield',
                    name: 'datetime_start_plan',
                    format: 'd.m.Y H:i',
                    fieldLabel: 'Время начала',
                    labelAlign: 'right',
                    readOnly: true,
                    labelWidth: 110
                },
                {
                    xtype: 'datefield',
                    name: 'datetime_finish_plan',
                    format: 'd.m.Y H:i',
                    fieldLabel: 'Время окончания',
                    labelAlign: 'right',
                    readOnly: true,
                    labelWidth: 110
                },
                {
                    xtype: 'textfield',
                    name: 'duration_plan',
                    fieldLabel: 'Длительность',
                    labelAlign: 'right',
                    readOnly: true,
                    labelWidth: 110
                },
                {
                    xtype: 'textfield',
                    name: 'distance_plan',
                    fieldLabel: 'Расстояние (км)',
                    labelAlign: 'right',
                    readOnly: true,
                    labelWidth: 110
                },
                {
                    xtype: 'textfield',
                    name: 'time_plan',
                    fieldLabel: 'Время (час)',
                    labelAlign: 'right',
                    readOnly: true,
                    labelWidth: 110
                },
                {
                    xtype: 'triggerfield',
                    fieldLabel: 'Заявка на ремонт',
                    name: 'repair_id',
                    labelAlign: 'right',
                    labelWidth: 110,
                    triggerCls: Ext.baseCSSPrefix + 'form-search-trigger',
                    onTriggerClick: function () {
                        if (this.value) {
                            var cRepair = Ext.create('app.controller.garage.repair.repair');
                            cRepair.open(this.value);
                        }
                    }
                }
            ]
        },
        {
            xtype: 'fieldset',
            title: 'Факт',
            padding: '2 10',
            margin: '5 10 10 0',
            flex: 5,
            items: [
                {
                    xtype: 'datefield',
                    name: 'datetime_start_fact',
                    format: 'd.m.Y H:i',
                    fieldLabel: 'Время начала',
                    labelAlign: 'right',
                    anchor: '100%',
                    readOnly: true,
                    labelWidth: 110
                },
                {
                    xtype: 'datefield',
                    name: 'datetime_finish_fact',
                    format: 'd.m.Y H:i',
                    fieldLabel: 'Время окончания',
                    labelAlign: 'right',
                    anchor: '100%',
                    readOnly: true,
                    labelWidth: 110
                },
                {
                    xtype: 'textfield',
                    name: 'duration_fact',
                    fieldLabel: 'Длительность',
                    labelAlign: 'right',
                    anchor: '100%',
                    readOnly: true,
                    labelWidth: 110
                },
                {
                    xtype: 'fieldset',
                    border: 0,
                    padding: 0,
                    margin: '0 0 5',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'km',
                            fieldLabel: 'Расстояние (км)',
                            labelAlign: 'right',
                            width: 180,
                            readOnly: true,
                            labelWidth: 110
                        },
                        {
                            xtype: 'button',
                            text: 'Путевой лист',
                            iconCls: 'icon_calculator',
                            padding: 2,
                            margin: '0 0 0 8',
                            handler: function () {
                                var form = this.up('form');
                                var rTrip = form.getRecord();
                                if (rTrip.get('id')) {
                                    var сTripFueling = Ext.create('app.controller.trip.fueling');
                                    var win = сTripFueling.open(rTrip.get('id'));
                                    win.on('close', function () {
                                        var dTripFueling = win.down('form').getValues();
                                        var start_km = parseInt(dTripFueling.start_km);
                                        var finish_km = parseInt(dTripFueling.finish_km);
                                        if (start_km && finish_km) {
                                            form.getForm().setValues({
                                                km: (finish_km - start_km)
                                            })
                                        }
                                    });
                                }
                            }
                        }
                    ]
                },
                {
                    xtype: 'textfield',
                    name: 'km_fact',
                    fieldLabel: 'Пробег (Глонасс)',
                    labelAlign: 'right',
                    width: 180,
                    hidden: true,
                    readOnly: true,
                    labelWidth: 110
                },
                {
                    xtype: 'fieldset',
                    border: 0,
                    padding: 0,
                    margin: '0 0 5',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'vehicle_repair_spent_time',
                            fieldLabel: 'Время на ТО',
                            labelAlign: 'right',
                            width: 180,
                            readOnly: true,
                            labelWidth: 110
                        },
                        {
                            xtype: 'hidden',
                            name: 'vehicle_repair_id'
                        },
                        {
                            xtype: 'button',
                            itemId: 'btn-vehicle_repair',
                            text: 'Был на ТО',
                            iconCls: 'icon_setting_tools',
                            padding: 2,
                            margin: '0 0 0 8',
                            handler: function () {
                                var form = this.up('form');
                                var rTrip = form.getRecord();
                                if (rTrip.get('id')) {
                                    var cVehicleRepair = Ext.create('app.controller.vehicle_repair');
                                    var vehicle_repair_id = form.getValues().vehicle_repair_id;
                                    var rVehicleRepair = Ext.create('app.model.vehicle.repair', {
                                        repair_date: rTrip.get('planning_date'),
                                        vehicle_id: rTrip.get('vehicle_id'),
                                        vehicle_title: rTrip.get('vehicle_title'),
                                        driver_id: rTrip.get('driver_id'),
                                        driver_user_name: rTrip.get('driver_name'),
                                        trip_id: rTrip.get('id')
                                    });
                                    var win = cVehicleRepair.open(vehicle_repair_id, rVehicleRepair);
                                    win.on('close', function () {
                                        var rVehicleRepair = win.down('form').getRecord();
                                        if (rVehicleRepair.get('id')) {
                                            form.getForm().setValues({
                                                vehicle_repair_id: rVehicleRepair.get('id'),
                                                vehicle_repair_spent_time: rVehicleRepair.get('spent_time_format')
                                            })
                                        }
                                    });
                                }
                            }
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    border: 0,
                    padding: 0,
                    margin: '0 0 5',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'driver_payment_sum',
                            fieldLabel: 'Начислено',
                            labelAlign: 'right',
                            width: 180,
                            readOnly: true,
                            labelWidth: 110
                        },
                        {
                            xtype: 'button',
                            text: 'Начисления',
                            iconCls: 'icon_table_money',
                            padding: 2,
                            margin: '0 0 0 8',
                            handler: function () {
                                var form = this.up('form');
                                var rTrip = form.getRecord();
                                var cTrip = Ext.create('app.controller.trip.main');
                                cTrip.openDriverPayment(rTrip);
                            }
                        }
                    ]
                }
            ]
        },
        {
            xtype: 'fieldset',
            title: 'Маршрут',
            padding: '2 10',
            margin: '5 10 10 10',
            flex: 7,
            items: [
                {
                    xtype: 'fieldset',
                    border: 0,
                    padding: 0,
                    margin: '0 0 5',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'numberfield',
                            name: 'id',
                            fieldLabel: 'Номер',
                            labelAlign: 'right',
                            flex: 1,
                            readOnly: true,
                            labelWidth: 60
                        },
                        {
                            xtype: 'datefield',
                            name: 'planning_date',
                            format: 'd.m.Y',
                            fieldLabel: 'Дата',
                            labelAlign: 'right',
                            flex: 1,
                            readOnly: true,
                            labelWidth: 60
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    border: 0,
                    padding: 0,
                    margin: '0 0 5',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'vehicle_trailer_title',
                            fieldLabel: 'ТС',
                            labelAlign: 'right',
                            flex: 1,
                            readOnly: true,
                            labelWidth: 60
                        },
                        {
                            xtype: 'textfield',
                            name: 'shift_title',
                            fieldLabel: 'Смена',
                            labelAlign: 'right',
                            flex: 1,
                            readOnly: true,
                            labelWidth: 60
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    border: 0,
                    padding: 0,
                    margin: '7 0',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'combobox',
                            fieldLabel: 'Водитель',
                            name: 'driver_name',
                            anchor: '100%',
                            labelWidth: 60,
                            labelAlign: 'right',
                            flex: 1,
                            typeAhead: true,
                            minChars: 3,
                            store: Ext.create('app.store.driver.main'),
                            displayField: 'user_name',
                            trigger2Cls: Ext.baseCSSPrefix + 'form-search-trigger',
                            listeners: {
                                beforequery: function (queryPlan) {
                                    queryPlan.combo.store.pageSize = 999;
                                    queryPlan.combo.store.proxy.extraParams = {status: 1};
                                },
                                select: function (combo, records) {
                                    var rec = records[0];
                                    this.up('form').getForm().setValues({
                                        driver_id: rec.get('id')
                                    });
                                }
                            },
                            onTrigger2Click: function () {
                                var driver_id = this.up('form').getForm().getValues().driver_id;
                                if (driver_id) {
                                    var cDriver = Ext.create('app.controller.driver');
                                    cDriver.open(driver_id);
                                }
                            }
                        },
                        {
                            xtype: 'hidden',
                            name: 'driver_id',
                            listeners: {
                                change: function () {
                                    var win = this.up('window');
                                    var form = this.up('form');
                                    var dTrip = form.getValues();
                                    // кнопка "Старт"
                                    if (dTrip.driver_id && (dTrip.state == 'scheduled' || dTrip.state == 'completed')) {
                                        win.query('#btn_change_state_performed')[0].enable();
                                    } else {
                                        win.query('#btn_change_state_performed')[0].disable();
                                    }
                                }
                            }
                        },
                        {
                            xtype: 'container',
                            layout: {
                                type: 'hbox',
                                pack: 'end'
                            },
                            width: 160,
                            items: [
                                {
                                    xtype: "displayfield",
                                    name: "vehicle_equipment_sim_number",
                                    submitValue: false,
                                    renderer: function (value) {
                                        return '<a href="#">' + value + '</a>';
                                    }
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: 'fieldset',
                    border: 0,
                    padding: 0,
                    margin: '7 0',
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'textfield',
                            name: 'state_title',
                            fieldLabel: 'Статус',
                            labelAlign: 'right',
                            flex: 1,
                            readOnly: true,
                            labelWidth: 60
                        },
                        {
                            xtype: 'hidden',
                            name: 'state',
                            listeners: {
                                change: function () {
                                    var win = this.up('window');
                                    var form = win.down('form').getForm();
                                    var rTrip = form.getRecord();

                                    // кнопка "Старт"
                                    if ((rTrip.get('driver_id') || rTrip.get('vehicle_is_hired')) && (this.value == 'scheduled' || this.value == 'completed' || this.value == 'in_repair')) {
                                        win.query('#btn_change_state_performed')[0].enable();
                                    } else {
                                        win.query('#btn_change_state_performed')[0].disable();
                                    }
                                    // кнопка "Завершить"
                                    if (this.value == 'performed' || this.value == 'checked' || this.value == 'in_repair') {
                                        win.query('#btn_change_state_completed')[0].enable();
                                    } else {
                                        win.query('#btn_change_state_completed')[0].disable();
                                    }
                                    // кнопка "Проверен"
                                    if (this.value == 'completed' || this.value == 'created') {
                                        win.query('#btn_change_state_checked')[0].enable();
                                    } else {
                                        win.query('#btn_change_state_checked')[0].disable();
                                    }
                                    // кнопка "Заменить ТС"
                                    if (this.value == 'scheduled') {
                                        win.query('#btn_replace_vehicle')[0].enable();
                                    } else {
                                        win.query('#btn_replace_vehicle')[0].disable();
                                    }
                                    // кнопка "В ремонт"
                                    if (this.value == 'performed') {
                                        win.query('#btn_change_state_in_repair')[0].enable();
                                    } else {
                                        win.query('#btn_change_state_in_repair')[0].disable();
                                    }
                                    // смена водителя
                                    if (rTrip.get('vehicle_is_hired')) {
                                        form.findField('driver_name').setReadOnly(true);
                                    } else {
                                        if (this.value == 'created' || this.value == 'scheduled' || this.value == 'performed' || this.value == 'completed') {
                                            form.findField('driver_name').setReadOnly(false);
                                        } else {
                                            form.findField('driver_name').setReadOnly(true);
                                        }
                                    }

                                    App.Global.enableElement(win.query('#btn_print_esm7')[0], this.value == 'checked'); // кнопка "Справка ЭСМ-7"
                                }
                            }
                        },
                        {
                            xtype: 'button',
                            text: 'История статусов',
                            iconCls: 'icon_clock_history_frame',
                            padding: 2,
                            margin: '0 0 0 10',
                            handler: function () {
                                var rTrip = this.up('form').getRecord();
                                if (rTrip.get('id')) {
                                    var win = Ext.widget('trip.win.state_history');
                                    var grid = win.down('grid');
                                    grid.store.proxy.extraParams.trip_id = rTrip.get('id');
                                    grid.store.load();
                                    win.show();
                                }
                            }
                        },
                        {
                            xtype: 'button',
                            iconCls: 'icon_battery_charge',
                            padding: 2,
                            margin: '0 0 0 10',
                            handler: function () {
                                var form = this.up('form');
                                var rTrip = form.getRecord();
                                var cVehicleEquipment = Ext.create('app.controller.vehicle_equipment');
                                cVehicleEquipment.openDeviceBatteryChart(rTrip);
                            }
                        }
                    ]
                },
                {
                    xtype: 'textareafield',
                    emptyText: 'Комментарий',
                    name: 'note',
                    anchor: '100%',
                    height: 45
                }
            ]
        }
    ]
});
