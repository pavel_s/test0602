Ext.define('app.view.trip.main.grid.points_for_tipper', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.trip.main.grid.points_for_tipper',
    columns: [
        {
            text: 'Код',
            dataIndex: 'id',
            width: 50,
            hidden: true
        },
        {
            text: 'Фикт.',
            dataIndex: 'is_fict',
            width: 40,
            align: 'center',
            renderer: function (val) {
                return val ? '&#10003;' : '';
            },
            field: {
                xtype: 'checkboxfield',
                inputValue: 1
            }
        },
        {
            text: 'п/п',
            dataIndex: 'n',
            width: 40,
            align: 'right',
            field: {
                xtype: 'numberfield',
                minValue: -9,
                maxValue: 99
            }
        },
        {
            xtype: 'actioncolumn',
            width: 20,
            align: 'center',
            sortable: false,
            menuDisabled: true,
            getClass: function (v, meta, rec) {
                return rec.get('iconCls');
            }
        },
        {
            text: 'Тип точки',
            dataIndex: 'type_title',
            width: 70
        },
        {
            xtype: 'actioncolumn',
            width: 20,
            align: 'center',
            sortable: false,
            menuDisabled: true,
            getClass: function (val, meta, rec) {
                return rec.get('iconCls_client_order_payment_method');
            }
        },
        {
            xtype: 'actioncolumn',
            width: 30,
            align: 'center',
            menuDisabled: true,
            getClass: function (v, meta, rec) {
                var rTrip = this.up('window').down('form').getRecord();
                if (rTrip.get('trailer_id')) {
                    if (rec.get('primary_trip_point_id')) {
                        return 'icon_baggage_cart_box';
                    }
                }
            }
        },
        {
            text: 'Заказ',
            width: 50,
            renderer: function (val, meta, rTripPoint) {
                return rTripPoint.get('is_repair') ? rTripPoint.get('repair_id') : rTripPoint.get('client_order_id');
            }
        },
        {
            text: 'Строка заказа',
            dataIndex: 'client_order_nomenclature_id',
            hidden: true,
            width: 50
        },
        {
            xtype: 'actioncolumn',
            width: 30,
            menuDisabled: true,
            sortable: false,
            align: 'center',
            getClass: function (view, meta, rTripPoint) {
                if (rTripPoint.get('is_repair')) {
                    return (rTripPoint.get('repair_id') && App.Global.checkRights('repair.view')) ? 'icon_bullet_magnify' : false;
                } else {
                    return (rTripPoint.get('client_order_id') && App.Global.checkRights('client_order.view', rTripPoint.get('client_order_manager_id'))) ? 'icon_bullet_magnify' : false;
                }
            },
            handler: function (grid, row) {
                var rTripPoint = grid.getStore().getAt(row);
                if (rTripPoint.get('is_repair') && rTripPoint.get('repair_id') && App.Global.checkRights('repair.view')) {
                    var cRepair = Ext.create('app.controller.garage.repair.repair');
                    cRepair.open(rTripPoint.get('repair_id'));
                }
                if (rTripPoint.get('client_order_id') && App.Global.checkRights('client_order.view', rTripPoint.get('client_order_manager_id'))) {
                    var cClientOrder = Ext.create('app.controller.client_order');
                    cClientOrder.open(rTripPoint.get('client_order_id'));
                }
            }
        },
        {
            text: 'Контрагент',
            dataIndex: 'contractor_title',
            flex: 1
        },
        {
            text: 'Номенклатура',
            dataIndex: 'client_order_nomenclature_title',
            flex: 1
        },
        {
            text: 'Адрес',
            dataIndex: 'contractor_delivery_title',
            flex: 2,
            renderer: function (value, metaData, rec) {
                var contractor_contact_name = rec.get('contractor_contact_name') ? rec.get('contractor_contact_name') + '<br>' : '';
                var contractor_contact_phone = rec.get('contractor_contact_phone') ? rec.get('contractor_contact_phone') + '<br>' : '';
                metaData.tdAttr = 'data-qtip="' + Ext.util.Format.htmlEncode(contractor_contact_name + '\n' + contractor_contact_phone) + '"';
                return value;
            },
            field: {
                xtype: 'triggerfield',
                trigger1Cls: Ext.baseCSSPrefix + 'form-point-trigger',
                onTrigger1Click: function () {
                    var grid = this.up('grid');
                    var rPoint = grid.getSelectionModel().getSelection()[0];

                    if (rPoint.get('type') == 'start' || rPoint.get('type') == 'finish') {
                        // выбор точек парковки
                        var cParking = Ext.create('app.controller.parking');
                        var winParking = cParking.openList();
                        var gridParking = winParking.down('grid');
                        for (var i in gridParking.events.itemdblclick.listeners) {
                            gridParking.removeListener('itemdblclick', gridParking.events.itemdblclick.listeners[i].fn)
                        }
                        gridParking.on('itemdblclick', function (view, rParking) {
                            rPoint.set({
                                parking_id: rParking.get('id'),
                                address: rParking.get('address'),
                                lat: rParking.get('lat'),
                                lng: rParking.get('lng')
                            });
                            rPoint.save();
                            grid.store.reload();
                            winParking.close();
                        });
                    } else {
                        // выбор точки поставщика
                        Ext.syncRequire([
                            'app.view.client_order.supplier_select.win',
                            'app.view.client_order.supplier_select.grid'
                        ]);
                        var rPointLoading = grid.store.getAt(rPoint.index - 1);
                        var win = Ext.widget('client_order.supplier_select.win');
                        win.nomenclature_id = rPoint.get('nomenclature_id');
                        win.contractor_delivery = {
                            id: rPoint.get('id'),
                            client_order_id: rPoint.get('client_order_id'),
                            lat: rPointLoading.get('lat'),
                            lng: rPointLoading.get('lng')
                        };
                        win.show();
                        // подстановка значений
                        var gridSupplier = win.down('grid');
                        gridSupplier.on('itemdblclick', function (view, rec) {
                            rPoint.set({
                                contractor_delivery_id: rec.get('contractor_delivery_id'),
                                contractor_delivery_title: rec.get('contractor_delivery_title'),
                                contractor_delivery_title_def: rec.get('contractor_delivery_title'),
                                lat: rec.get('contractor_delivery_lat'),
                                lng: rec.get('contractor_delivery_lng')
                            });
                            rPoint.save();
                            grid.store.reload();
                            win.close();
                        });
                    }
                }
            }
        },
        {
            text: 'Кол-во П',
            dataIndex: 'volume',
            width: 70,
            align: 'right',
            renderer: function (value, metaData, rec) {
                return value ? (value + ' ' + rec.get('unit_title')) : '-';
            },
            field: {
                xtype: 'numberfield',
                minValue: 1,
                maxValue: 99,
                decimalPrecision: 2
            }
        },
        {
            text: 'Кол-во Ф',
            dataIndex: 'volume_fict',
            width: 70,
            align: 'right',
            tdCls: 'background_grey_blue',
            renderer: function (value, metaData, rec) {
                if (rec.get('volume_fict') == 0 && rec.get('volume_fact') == 0) {
                    return '-';
                } else {
                    value = rec.get('volume_fict') > 0 && rec.get('volume_fict') != rec.get('volume_fact') ? '<span style="color:#e00">' + rec.get('volume_fict') + '</span>' : rec.get('volume_fact');
                    return value + ' ' + rec.get('unit_title');
                }
            },
            field: {
                xtype: 'numberfield',
                minValue: 1,
                maxValue: 99,
                decimalPrecision: 2
            }
        },
        {
            xtype: 'actioncolumn',
            itemId: 'trip_point_files',
            width: 30,
            align: 'center',
            sortable: false,
            menuDisabled: true,
            getClass: function (view, meta, rTripPoint) {
                if (rTripPoint.get('file_cnt') > 0) {
                    meta.tdAttr = 'data-qtip="Фото накладной"';
                    return 'icon_camera';
                } else {
                    return false;
                }
            },
            handler: function (view, row) {
                var grid = this.up('grid');
                var rTripPoint = grid.store.getAt(row);
                if (rTripPoint.get('file_cnt') > 0) {
                    window.open('/trip/point_files/' + rTripPoint.get('id'), '_blank');
                }
            }
        },
        {
            xtype: 'datecolumn',
            text: 'Прибытие (план)',
            dataIndex: 'datetime_arrival',
            hidden: true,
            width: 100,
            format: 'd.m.Y H:i'
        },
        {
            xtype: 'datecolumn',
            text: 'Убытие (план)',
            dataIndex: 'datetime_departure',
            hidden: true,
            width: 100,
            format: 'd.m.Y H:i'
        },
        {
            xtype: 'actioncolumn',
            menuText: 'Фото кузова',
            width: 30,
            align: 'center',
            sortable: false,
            menuDisabled: true,
            hidden: true,
            getClass: function (view, meta, rTripPoint) {
                if (rTripPoint.get('datetime_arrival_fact') && (rTripPoint.get('type') == 'unloading' || rTripPoint.get('type') == 'loading')) {
                    meta.tdAttr = 'data-qtip="Фото кузова"';
                    return 'icon_cctv_camera';
                } else {
                    return false;
                }
            },
            handler: function (view, row) {
                var grid = this.up('grid');
                var rTripPoint = grid.store.getAt(row);
                Ext.Ajax.request({
                    url: '/trip/vehicle_body_photo/' + rTripPoint.get('id'),
                    method: 'GET',
                    success: function (response) {
                        var responseData = Ext.JSON.decode(response.responseText);
                        if (responseData.success) {
                            window.open(responseData.url, '_blank');
                        } else {
                            Ext.Msg.alert('', responseData.errors.join('\n'));
                        }
                    }
                });
            }
        },
        {
            xtype: 'datecolumn',
            text: 'Выполнение',
            dataIndex: 'datetime_arrival_fact',
            width: 85,
            format: 'd.m H:i',
            field: {
                xtype: 'datefield',
                format: 'd.m H:i'
            }
        },
        {
            text: 'Часы',
            dataIndex: 'downtime_format',
            width: 55,
            field: {
                xtype: 'textfield',
                plugins: [new Ext.ux.InputTextMask('99:99', true)]
            }
        },
        {
            text: 'ТТН',
            dataIndex: 'ttn',
            width: 70,
            field: {
                xtype: 'textfield'
            }
        },
        {
            xtype: 'actioncolumn',
            itemId: 'document_closing',
            width: 30,
            align: 'center',
            sortable: false,
            getClass: function (v, meta, rec) {
                if (rec.get('contractor_type') == 'client' && rec.get('document_closing')
                    && (rec.get('nomenclature_delivery_type') != 'client_client' || rec.get('type') == 'loading')
                ) {
                    meta.tdAttr = 'data-qtip="Закрывающие документы"';
                    return 'icon_blue_document_exclamation';
                }
                return false;
            },
            handler: function (view, row) {
                var grid = this.up('grid');
                var rec = grid.store.getAt(row); // выбранная точка
                var сTrip = Ext.create('app.controller.trip.main');
                сTrip.getContractorDocumentClosing(rec);
            }
        },
        {
            xtype: 'numbercolumn',
            text: 'Тариф',
            align: 'right',
            width: 70,
            renderer: function (value, metaData, rec) {
                value = rec.get('driver_payment_sum') > 0 ? rec.get('driver_payment_sum') : (rec.get('tariff_bp') ? rec.get('tariff_driver') : rec.get('tariff_driver_ext'));
                return Ext.util.Format.number(value, '0,000.00');
            }
        },
        {
            text: 'Удержание',
            itemId: 'col-driver_fine',
            dataIndex: 'driver_fine_title',
            field: {
                xtype: 'combobox',
                editable: false,
                multiSelect: true,
                store: Ext.create('app.store.dictionary.driver_fine'),
                displayField: 'title',
                listConfig: {
                    getInnerTpl: function () {
                        return '<div class="x-combo-list-item"><img src="' + Ext.BLANK_IMAGE_URL + '" class="chkCombo-default-icon chkCombo" width="16" /> {title}</div>';
                    }
                },
                listeners: {
                    beforequery: function (queryPlan) {
                        queryPlan.combo.store.pageSize = 999;
                        queryPlan.combo.store.proxy.extraParams.status = 1;    // только активные
                    },
                    expand: function () {
                        if (this.value.length) {
                            this.setValue(this.value[0].split(', '));
                        }
                    },
                    beforeselect: function (combo, rDriverFine) {
                        var grid = this.up('grid');
                        var rTripPoint = grid.getSelectionModel().getSelection()[0];
                        Ext.Ajax.request({
                            url: '/trip/driver_fine',
                            method: 'POST',
                            params: {
                                trip_point_id: rTripPoint.get('id'),
                                driver_fine_id: rDriverFine.get('id')
                            }
                        });
                    },
                    beforedeselect: function (combo, rDriverFine) {
                        var grid = this.up('grid');
                        var rTripPoint = grid.getSelectionModel().getSelection()[0];
                        Ext.Ajax.request({
                            url: '/trip/driver_fine',
                            method: 'DELETE',
                            params: {
                                trip_point_id: rTripPoint.get('id'),
                                driver_fine_id: rDriverFine.get('id')
                            }
                        });
                    }
                }
            }
        },
        {
            text: 'Б/П',
            dataIndex: 'tariff_bp',
            width: 30,
            align: 'center',
            renderer: function (val) {
                return val ? '&#10003;' : '';
            },
            field: {
                xtype: 'checkboxfield',
                inputValue: 1
            }
        },
        {
            xtype: 'numbercolumn',
            text: 'Тариф без коэф.',
            dataIndex: 'tariff_driver',
            align: 'right',
            width: 70,
            hidden: true
        },
        {
            xtype: 'actioncolumn',
            itemId: 'change_trip',
            width: 30,
            align: 'center',
            sortable: false,
            menuDisabled: true,
            getClass: function (v, meta, rec) {
                var rTrip = this.up('window').down('form').getRecord();
                if (rec.get('is_confirm') || rTrip.get('state') == 'checked' || !App.Global.checkRights('trip.edit_points')) {
                    return false;
                }
                if (rec.get('type') == 'loading' && rec.get('state') != 'done') {
                    return 'icon_tipper';
                }
                if (rec.get('type') == 'unloading' && rec.get('trip_id') != rec.get('loading_trip_point_trip_id') && rec.get('loading_trip_point_trip_id')) {
                    return 'icon_arrow_rotate_clockwise';
                }
                return false;
            },
            handler: function (view, row) {
                var grid = this.up('grid');
                var rec = grid.store.getAt(row); // выбранная точка
                var rPointLoading = grid.store.getAt(row); // точка загрузки
                var rPointUnloading = grid.store.getAt(row + 1); // точка разгрузки
                var rTrip = this.up('window').down('form').getRecord();

                // блокировка редактирования
                if (rec.get('is_confirm') || rTrip.get('state') == 'checked' || !App.Global.checkRights('trip.edit_points')) {
                    return false;
                }

                // перенос точки в другой рейс
                if (rPointLoading.get('type') == 'loading' && rPointLoading.get('state') != 'done') {
                    var winTrip = Ext.widget('trip.win.change_trip_for_point');
                    var gridTrip = winTrip.down('grid');
                    winTrip.show();

                    // фильтры для стора
                    Ext.ModelManager.getModel('app.model.trip.main').load(rPointLoading.get('trip_id'), {
                        success: function (rTrip) {
                            gridTrip.store.proxy.extraParams.states = '["scheduled", "performed"]';
                            gridTrip.store.proxy.extraParams.not_id = '[' + rTrip.get('id') + ']';
                            gridTrip.store.proxy.extraParams.max_volume = rTrip.get('max_volume');
                            gridTrip.store.proxy.extraParams.max_weight = rTrip.get('max_weight');
                            gridTrip.query('#trip_date')[0].setValue(rTrip.get('planning_date'));
                        }
                    });

                    // выбор другого рейса
                    gridTrip.on('itemdblclick', function (view, rTrip) {
                        // последовательно переносим загрузку и разгрузку
                        rPointLoading.set('trip_id', rTrip.get('id'));
                        rPointLoading.save({
                            success: function () {
                                // перенос второй точки
                                rPointUnloading.set('trip_id', rTrip.get('id'));
                                rPointUnloading.save({
                                    success: function () {
                                        // удаление точек из списка
                                        grid.store.remove(rPointLoading);
                                        grid.store.remove(rPointUnloading);
                                        // закрываем окно переноса
                                        winTrip.close();
                                    },
                                    failure: function (record, operation) {
                                        var errors = operation.request.scope.reader.jsonData["errors"];
                                        console.log(errors);
                                    }
                                });
                            },
                            failure: function (record, operation) {
                                var errors = operation.request.scope.reader.jsonData["errors"];
                                console.log(errors);
                            }
                        });
                    });
                }

                // возврат точки в ее рейс
                if (rec.get('type') == 'unloading' && rec.get('trip_id') != rec.get('loading_trip_point_trip_id') && rec.get('loading_trip_point_trip_id')) {
                    Ext.ModelManager.getModel('app.model.trip.main').load(rec.get('loading_trip_point_trip_id'), {
                        success: function (rLoadingTrip) {
                            if (rLoadingTrip.get('state') == 'checked') {
                                Ext.Msg.alert('Внимание!', 'Возврат точки в рейс ' + rLoadingTrip.get('id') + ' невозможен.<br>Рейс ' + rLoadingTrip.get('id') + ' находится в статусе "Проверен".');
                            } else {
                                Ext.Msg.show({
                                    msg: 'Вернуть точку в рейс ' + rec.get('loading_trip_point_trip_id') + '?',
                                    buttons: Ext.Msg.YESNO,
                                    fn: function (btn) {
                                        if (btn == 'yes') {
                                            rec.set('trip_id', rec.get('loading_trip_point_trip_id'));
                                            rec.set('n', null);
                                            rec.save({
                                                success: function () {
                                                    grid.store.loadPage(1);
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            }
        },
        {
            xtype: 'actioncolumn',
            itemId: 'delete',
            width: 30,
            align: 'center',
            sortable: false,
            menuDisabled: true,
            getClass: function (v, meta, rec) {
                var rTrip = this.up('window').down('form').getRecord();
                if (rTrip.get('state') == 'checked' || !App.Global.checkRights('trip.edit_points')) {
                    return false;
                }
                if (rec.get('is_confirm')) {
                    return 'icon_lock';
                } else {
                    if (rec.get('client_order_nomenclature_id') || rec.get('repair_id')) {
                        if (rec.get('state') == 'done') {
                            return 'icon_broom';
                        } else {
                            if (rec.get('loading_trip_point_id')) {
                                return !rec.get('loading_trip_point_is_fict') ? 'icon_document_prepare' : false;
                            } else {
                                return 'icon_delete';
                            }
                        }
                    } else {
                        return false;
                    }
                }
            },
            handler: function (view, row) {
                var self = this;
                var grid = this.up('grid');
                var rec = grid.store.getAt(row);
                var rTripPoint = grid.store.getAt(row); // точка рейса
                var rTrip = this.up('window').down('form').getRecord();

                // блокировка редактирования
                if (rec.get('is_confirm') || rTrip.get('state') == 'checked' || !App.Global.checkRights('trip.edit_points')) {
                    return false;
                }

                if (rec.get('client_order_nomenclature_id') || rec.get('repair_id')) {
                    // очистка данных, пришедших от водителя
                    if (rec.get('state') == 'done') {
                        Ext.Msg.show({
                            msg: 'Вы действительно хотите очистить данные, полученые от водителя?',
                            buttons: Ext.Msg.YESNO,
                            fn: function (btn) {
                                if (btn == 'yes') {
                                    rec.set({
                                        state: 'plan',
                                        datetime_arrival_fact: null,
                                        datetime_departure_fact: null,
                                        volume_fact: null
                                    });
                                    grid.store.sync();
                                }
                            }
                        });
                    } else {
                        // удаление точки
                        if (!rec.get('loading_trip_point_id')) {
                            Ext.Ajax.request({
                                url: '/trip/point/' + rTripPoint.get('id'),
                                method: 'DELETE',
                                params: {
                                    delete_cause: cause
                                },
                                success: function (result) {
                                    var data = JSON.parse(result.responseText);
                                    if (data.success) {
                                        grid.store.reload();
                                    }
                                }
                            });
                            return true;
                        }
                        // замена заказа
                        if (rec.get('loading_trip_point_id') && !rec.get('loading_trip_point_is_fict')) {
                            Ext.syncRequire([
                                'app.view.trip.select_position.tipper.win',
                                'app.view.trip.select_position.tipper.grid'
                            ]);

                            var winClientOrderPosition = Ext.widget('trip.select_position.tipper.win');
                            var gridClientOrderPosition = winClientOrderPosition.down('grid');
                            winClientOrderPosition.show();

                            gridClientOrderPosition.store.proxy.extraParams = {
                                nomenclature_id: rec.get('nomenclature_id'),
                                planning_date: Ext.Date.format(rTrip.get('planning_date'), 'Y-m-d'),
                                closed: 1
                            };
                            gridClientOrderPosition.store.loadPage(1);

                            gridClientOrderPosition.on('itemdblclick', function (view, rPosition) {
                                Ext.Ajax.request({
                                    url: '/trip_point/change_client_order_position',
                                    params: {
                                        trip_point_id: rec.get('id'),
                                        client_order_nomenclature_id: rPosition.get('client_order_nomenclature_id'),
                                        contractor_id: rPosition.get('contractor_id'),
                                        contractor_delivery_id: rPosition.get('point_to_contractor_delivery_id')
                                    },
                                    success: function (result) {
                                        var data = JSON.parse(result.responseText);
                                        if (data.success) {
                                            grid.store.reload();
                                            winClientOrderPosition.close();
                                        }
                                    }
                                });
                            });
                        }
                    }
                }
            }
        }
    ],
    initComponent: function () {
        this.store = Ext.create('app.store.trip.point');
        this.callParent(arguments);
    },
    dockedItems: [
        {
            xtype: 'toolbar',
            itemId: 'toolbar_top',
            dock: 'top',
            items: [
                {
                    text: 'Добавить',
                    itemId: 'btn_add',
                    iconCls: 'icon_add',
                    padding: 5,
                    handler: function () {
                        Ext.syncRequire([
                            'app.view.trip.select_position.tipper.win',
                            'app.view.trip.select_position.tipper.grid'
                        ]);

                        var win = this.up('window');
                        var form = win.down('form');
                        var grid = win.query('grid:not([hidden])')[0];
                        var rTrip = form.getRecord();

                        var winClientOrderPosition = Ext.widget('trip.select_position.tipper.win');
                        var gridClientOrderPosition = winClientOrderPosition.down('grid');
                        winClientOrderPosition.query('#toolbar_top')[0].hide();
                        winClientOrderPosition.show();

                        gridClientOrderPosition.store.proxy.extraParams = {
                            planning_date: Ext.Date.format(rTrip.get('planning_date'), 'Y-m-d'),
                            type: 'tipper',
                            closed: 1
                        };
                        gridClientOrderPosition.store.loadPage(1);

                        gridClientOrderPosition.on('itemdblclick', function (view, rPosition) {
                            if (rPosition.get('can_add_to_trip') && !rPosition.get('disable_planning')) {
                                var rLastTripPoint = grid.getLastTripPoint();
                                // распределение объема
                                var total_volume = rTrip.get('max_volume') + rTrip.get('trailer_volume');
                                var volumeVehicle = Math.ceil(1 + (rTrip.get('volume') - total_volume) / total_volume) * rTrip.get('max_volume'); // объем загрузки на тс
                                var volumeTrailer = rTrip.get('volume') - volumeVehicle; // объем загрузки на прицеп
                                // маршрут до точки
                                var points = [
                                    (rLastTripPoint.get('lat') && rLastTripPoint.get('lng') ? [rLastTripPoint.get('lat'), rLastTripPoint.get('lng')] : [55.76, 37.64]),
                                    [rPosition.get('point_from_lat'), rPosition.get('point_from_lng')]
                                ];
                                ymaps.route(points).then(function (route) {
                                    var timeRoute0 = Math.ceil(route.getTime() / 60 * parseFloat(App.Global.settings.tipper_ratio_time));
                                    var timeLoading = parseInt(App.Global.settings.tipper_time_loading) * (rTrip.get('trailer_id') ? 2 : 1); // время загрузки
                                    var timeUnloading = parseInt(App.Global.settings.tipper_time_unloading) * (rTrip.get('trailer_id') ? 2 : 1); // время разгрузки
                                    // маршрут между точками
                                    var points = [
                                        [rPosition.get('point_from_lat'), rPosition.get('point_from_lng')],
                                        [rPosition.get('point_to_lat'), rPosition.get('point_to_lng')]
                                    ];
                                    ymaps.route(points).then(function (route) {
                                        var timeRoute1 = Math.ceil(route.getTime() / 60 * parseFloat(App.Global.settings.tipper_ratio_time));
                                        // создаем точку загрузки
                                        var rTripPointLoading = Ext.create('app.model.trip.point', {
                                            trip_id: rTrip.get('id'),
                                            client_order_nomenclature_id: rPosition.get('client_order_nomenclature_id'),
                                            volume: volumeVehicle,
                                            address: rPosition.get('point_from_address'),
                                            lat: rPosition.get('point_from_lat'),
                                            lng: rPosition.get('point_from_lng'),
                                            contractor_delivery_id: rPosition.get('point_from_contractor_delivery_id'),
                                            datetime_arrival: rLastTripPoint.get('datetime_departure') ? Ext.Date.add(rLastTripPoint.get('datetime_departure'), Ext.Date.MINUTE, timeRoute0) : null,
                                            datetime_departure: rLastTripPoint.get('datetime_departure') ? Ext.Date.add(rLastTripPoint.get('datetime_departure'), Ext.Date.MINUTE, timeRoute0 + timeLoading) : null,
                                            type: 'loading'
                                        });
                                        rTripPointLoading.save({
                                            success: function (rTripPointLoading) {
                                                // создаем точку разгрузки
                                                var rTripPointUnloading = Ext.create('app.model.trip.point', {
                                                    trip_id: rTrip.get('id'),
                                                    loading_trip_point_id: rTripPointLoading.get('id'),
                                                    client_order_nomenclature_id: rPosition.get('client_order_nomenclature_id'),
                                                    volume: volumeVehicle,
                                                    address: rPosition.get('point_to_address'),
                                                    lat: rPosition.get('point_to_lat'),
                                                    lng: rPosition.get('point_to_lng'),
                                                    contractor_delivery_id: rPosition.get('point_to_contractor_delivery_id'),
                                                    datetime_arrival: rTripPointLoading.get('datetime_departure') ? Ext.Date.add(rTripPointLoading.get('datetime_departure'), Ext.Date.MINUTE, timeRoute1) : null,
                                                    datetime_departure: rTripPointLoading.get('datetime_departure') ? Ext.Date.add(rTripPointLoading.get('datetime_departure'), Ext.Date.MINUTE, timeRoute1 + timeUnloading) : null,
                                                    type: 'unloading'
                                                });
                                                rTripPointUnloading.save({
                                                    success: function () {
                                                        if (rTrip.get('trailer_id')) {
                                                            var rTripPointLoadingTrailer = Ext.create('app.model.trip.point', {
                                                                trip_id: rTrip.get('id'),
                                                                primary_trip_point_id: rTripPointLoading.get('id'),
                                                                client_order_nomenclature_id: rPosition.get('client_order_nomenclature_id'),
                                                                volume: volumeTrailer,
                                                                address: rPosition.get('point_from_address'),
                                                                lat: rPosition.get('point_from_lat'),
                                                                lng: rPosition.get('point_from_lng'),
                                                                contractor_delivery_id: rPosition.get('point_from_contractor_delivery_id'),
                                                                datetime_arrival: rTripPointLoading.get('datetime_arrival'),
                                                                datetime_departure: rTripPointLoading.get('datetime_departure'),
                                                                type: 'loading'
                                                            });
                                                            rTripPointLoadingTrailer.save({
                                                                success: function (rTripPointLoadingTrailer) {
                                                                    var rTripPointUnloadingTrailer = Ext.create('app.model.trip.point', {
                                                                        trip_id: rTrip.get('id'),
                                                                        primary_trip_point_id: rTripPointUnloading.get('id'),
                                                                        loading_trip_point_id: rTripPointLoadingTrailer.get('id'),
                                                                        client_order_nomenclature_id: rPosition.get('client_order_nomenclature_id'),
                                                                        volume: volumeTrailer,
                                                                        address: rPosition.get('point_to_address'),
                                                                        lat: rPosition.get('point_to_lat'),
                                                                        lng: rPosition.get('point_to_lng'),
                                                                        contractor_delivery_id: rPosition.get('point_to_contractor_delivery_id'),
                                                                        datetime_arrival: rTripPointUnloading.get('datetime_arrival'),
                                                                        datetime_departure: rTripPointUnloading.get('datetime_departure'),
                                                                        type: 'unloading'
                                                                    });
                                                                    rTripPointUnloadingTrailer.save({
                                                                        success: function (rTripPointUnloadingTrailer) {
                                                                            grid.store.reload();
                                                                            winClientOrderPosition.close();
                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        } else {
                                                            grid.store.reload();
                                                            winClientOrderPosition.close();
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                    });
                                });
                            } else {
                                if (!rPosition.get('can_add_to_trip')) {
                                    Ext.Msg.alert('Внимание!', 'Для добавления позиции в рейс, необходимо указать тариф.');
                                }
                                if (rPosition.get('disable_planning')) {
                                    Ext.Msg.alert('Внимание!', 'Планирование запрещено! Превышен лимит долга.');
                                }
                            }
                        });
                    }
                },
                {
                    text: 'Добавить точку разгрузки',
                    iconCls: 'icon_add',
                    padding: 5,
                    handler: function () {
                        var win = this.up('window');
                        var form = win.down('form');
                        var grid = win.query('grid:not([hidden])')[0];
                        var rTrip = form.getRecord();
                        Ext.Ajax.request({
                            method: 'GET',
                            url: '/trip/check_unloading_last_trip/' + rTrip.get('id'),
                            params: {move: 1},
                            success: function (result) {
                                var data = JSON.parse(result.responseText);
                                if (data.success) {
                                    grid.store.reload();
                                }
                            }
                        });
                    }
                },
                {
                    text: 'Добавить ремонт',
                    iconCls: 'icon_add',
                    padding: 5,
                    handler: function () {
                        var grid = this.up('window').query('grid:not([hidden])')[0];
                        var cTrip = Ext.create('app.controller.trip.main');
                        var rTrip = this.up('window').down('form').getRecord();
                        var winRepair = cTrip.openListRepair(rTrip);
                        var gridRepair = winRepair.down('grid');
                        var rLastTripPoint = grid.getLastTripPoint();

                        gridRepair.on('itemdblclick', function (view, rRepair) {
                            Ext.Ajax.request({
                                url: '/garage/repair/planning/' + rRepair.get('id'),
                                success: function (result) {
                                    var data = JSON.parse(result.responseText);
                                    if (data.success) {
                                        grid.store.reload();
                                        winRepair.close();
                                    } else {
                                        Ext.Msg.alert('', data.errors.join('\n'));
                                    }
                                }
                            });
                        });
                    }
                },
                '->',
                {
                    xtype: 'button',
                    text: 'Удержание',
                    itemId: 'btn-driver_fine',
                    iconCls: 'icon_coins_in_hand',
                    padding: 5,
                    enableToggle: true,
                    handler: function () {
                        var grid = this.up('grid');
                        grid.query('#col-driver_fine')[0].setVisible(this.pressed);
                    }
                },
                {
                    text: 'Внести оплату',
                    itemId: 'btn_payment',
                    iconCls: 'icon_total_plan_cost',
                    padding: 5,
                    handler: function () {
                        var win = this.up('window');
                        var form = win.down('form');
                        var rTrip = form.getRecord();
                        var cTrip = Ext.create('app.controller.trip.main');
                        cTrip.openPayment(rTrip);
                    }
                },
                {
                    text: 'Заменить ТС',
                    itemId: 'btn_replace_vehicle',
                    iconCls: 'icon_tipper',
                    padding: 5,
                    handler: function () {
                        var win = this.up('window');
                        var grid = this.up('grid');
                        var rTrip = win.down('form').getRecord();

                        var winTrip = Ext.widget('trip.win.change_trip_for_point');
                        var gridTrip = winTrip.down('grid');
                        gridTrip.query('#filter_date_shift')[0].hide();
                        winTrip.show();

                        // фильтры для стора
                        gridTrip.store.proxy.extraParams = {
                            states: '["scheduled", "confirmed"]',
                            not_id: '[' + rTrip.get('id') + ']',
                            planning_date: rTrip.get('planning_date'),
                            shift_id: rTrip.get('shift_id'),
                            status: 1
                        }
                        gridTrip.store.loadPage(1);


                        // выбор другого рейса
                        gridTrip.on('itemdblclick', function (view, rTripTo) {
                            Ext.Msg.show({
                                msg: 'Вы действительно хотите обменять маршруты у ТС ' + rTrip.get('vehicle_title') + ' и ТС ' + rTripTo.get('vehicle_title') + '?',
                                buttons: Ext.Msg.YESNO,
                                fn: function (btn) {
                                    if (btn == 'yes') {
                                        Ext.Ajax.request({
                                            url: '/trip/replace_vehicle',
                                            params: {
                                                trip1_id: rTrip.get('id'),
                                                trip2_id: rTripTo.get('id')
                                            },
                                            success: function (result) {
                                                var data = JSON.parse(result.responseText);
                                                if (data.success) {
                                                    grid.store.reload();
                                                    winTrip.close();
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        });
                    }
                }
            ]
        }
    ],
    plugins: [
        {
            ptype: 'cellediting',
            clicksToEdit: 1,
            listeners: {
                beforeEdit: function (editor, e) {
                    var grid = e.grid.up('window').query('grid:not([hidden])')[0];
                    var rTrip = e.grid.up('window').down('form').getRecord();
                    var rPoint = e.record;
                    // тафифф без коэффициента
                    if (e.field == 'tariff_bp') {
                        if (App.Global.checkRights('trip.check')) {
                            return rPoint.get('tariff_driver') != rPoint.get('tariff_driver_ext') ? true : false;
                        } else {
                            return false;
                        }
                    }
                    // права на редактирование точек
                    if (!App.Global.checkRights('trip.edit_points')) {
                        return false;
                    }
                    // право на редактирвоание удержаний по рейсу
                    if (e.field == 'driver_fine_title' && !App.Global.checkRights('trip.driver_fine.edit')) {
                        return false;
                    }
                    // блокировка при статусе рейса "Проверено"
                    if (rTrip.get('state') == 'checked') {
                        return false;
                    }
                    // проверка на фиктивную точку загруки
                    if (rPoint.get('loading_trip_point_is_fict')) {
                        return false;
                    }
                    // порядок сортировки
                    if (e.field == 'n') {
                        return rPoint.get('type') == 'loading' || rPoint.get('type') == 'start_rent' || rPoint.get('type') == 'start_repair';
                    }
                    // плановый фиктивный объем
                    if (e.field == 'volume') {
                        return rPoint.get('type') != 'start' && rPoint.get('type') != 'finish' && rPoint.get('type') != 'start_rent' && rPoint.get('type') != 'start_repair';
                    }
                    // фиктивный объем
                    if (e.field == 'volume_fict') {
                        return rPoint.get('type') != 'start' && rPoint.get('type') != 'finish' && rPoint.get('type') != 'start_rent' && rPoint.get('type') != 'start_repair';
                    }
                    // адреса для позиций "клиент-постащик" и точки старта и финиша
                    if (e.field == 'contractor_delivery_title') {
                        return (rPoint.get('nomenclature_delivery_type') == 'client_supplier' && rPoint.get('type') == 'unloading')
                            || (rPoint.get('nomenclature_delivery_type') == 'supplier_client' && rPoint.get('type') == 'loading')
                            || rPoint.get('type') == 'start'
                            || rPoint.get('type') == 'finish';
                    }
                    // признак "фиктивный" можно установить только на строке с загрузкой
                    if (e.field == 'is_fict') {
                        return rPoint.get('type') == 'loading' ? true : false;
                    }
                    // ТТН только для точек загрузка/разгрузка в точках клиента
                    if (e.field == 'ttn') {
                        return (rPoint.get('type') == 'loading' || rPoint.get('type') == 'unloading') &&
                            rPoint.get('contractor_type') == 'client' &&
                            rPoint.get('is_duty_point');
                    }
                    return true;
                },
                edit: function (editor, e) {
                    var grid = e.grid;
                    grid.store.sync({
                        success: function () {
                            grid.store.reload();
                        },
                        failure: function () {
                            var errors = this.getReader().jsonData["errors"];
                            console.log(errors);
                        }
                    });
                }
            }
        }
    ],
    viewConfig: {
        getRowClass: function (rec) {
            var rTrip = this.up('window').down('form').getRecord();
            var factVolume = rec.get('volume_fict') ? rec.get('volume_fict') : rec.get('volume_fact');
            var maxVolume = rec.get('unit_title') == 'т' ? rTrip.get('max_weight') : rTrip.get('max_volume');
            var clsDone = rec.get('state') == 'done' || rec.get('is_fict') ? 'active-row' : 'no-active-row';
            var clsMaxLoading = factVolume > 0 && maxVolume > 0 && factVolume >= maxVolume * App.Global.settings.rate_max_loading ? 'bg-danger' : '';
            return clsDone + ' ' + clsMaxLoading;
        }
    },

    /**
     * Получение предыдущей точки
     */
    getLastTripPoint: function (rTripPoint) {
        var pos = rTripPoint ? this.store.indexOf(rTripPoint) : this.store.getCount();
        // нахождение предыдущей точки
        for (var i = pos - 1; i >= 0; i--) {
            for (var i = pos - 1; i >= 0; i--) {
                if (this.store.getAt(i).get('type') != 'finish') {
                    return this.store.getAt(i);
                }
            }
        }
        return null;
    }
});