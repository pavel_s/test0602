Ext.define('app.view.trip.main.grid.points_for_container', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.trip.main.grid.points_for_container',
    columns: [
        {
            text: 'Код',
            dataIndex: 'id',
            width: 50,
            hidden: true
        },
        {
            text: 'п/п',
            dataIndex: 'n',
            width: 40,
            align: 'right',
            field: {
                xtype: 'numberfield',
                minValue: 1,
                maxValue: 99
            }
        },
        {
            xtype: 'actioncolumn',
            width: 20,
            align: 'center',
            sortable: false,
            menuDisabled: true,
            getClass: function (v, meta, rec) {
                return rec.get('iconCls');
            }
        },
        {
            text: 'Тип точки',
            dataIndex: 'type_title',
            width: 70
        },
        {
            xtype: 'actioncolumn',
            width: 20,
            align: 'center',
            sortable: false,
            menuDisabled: true,
            getClass: function (val, meta, rec) {
                return rec.get('iconCls_client_order_payment_method');
            }
        },
        {
            xtype: 'actioncolumn',
            width: 30,
            align: 'center',
            menuDisabled: true,
            getClass: function (v, meta, rec) {
                var rTrip = this.up('window').down('form').getRecord();
                if (rTrip.get('trailer_id')) {
                    if (rec.get('primary_trip_point_id')) {
                        return 'icon_baggage_cart_box';
                    }
                    if (!rec.get('has_trailer') && (rec.get('type') == 'loading' || rec.get('type') == 'unloading')) {
                        return 'icon_add';
                    }
                }
            },
            handler: function (view, row) {
                var grid = this.up('grid');
                var rTripPoint = grid.getStore().getAt(row);
                var rTrip = this.up('window').down('form').getRecord();
                if (rTrip.get('trailer_id')) {
                    // планирование точки прицепа
                    if (rTripPoint.get('primary_trip_point_id')) {
                        Ext.syncRequire([
                            'app.view.trip.select_position.container.win',
                            'app.view.trip.select_position.container.grid_contractor',
                            'app.view.trip.select_position.container.grid_parking',
                            'app.view.trip.select_position.container.grid_position'
                        ]);

                        var rLastTripPoint = grid.getLastTripPoint(rTripPoint);

                        var winSelectPosition = Ext.widget('trip.select_position.container.win');
                        var gridContractor = winSelectPosition.query('#contractor')[0];
                        var gridParking = winSelectPosition.query('#parking')[0];
                        var gridPosition = winSelectPosition.query('#position')[0];
                        winSelectPosition.show();

                        // свалки
                        gridContractor.store.proxy.extraParams = {
                            supplier_nomenclature_delivery_type: 'container',
                            status: 1
                        };
                        gridContractor.store.loadPage(1);

                        // парковки
                        gridParking.store.proxy.extraParams = {
                            status: 1
                        };
                        gridParking.store.loadPage(1);

                        // позиции
                        gridPosition.store.proxy.extraParams = {
                            planning_date: Ext.Date.format(rTrip.get('planning_date'), 'Y-m-d'),
                            type: 'container',
                            closed: 1
                        };
                        gridPosition.store.loadPage(1);

                        // выбор свалки
                        gridContractor.on('itemdblclick', function (view, rContractorDelivery) {
                            rTripPoint.set({
                                loading_trip_point_id: null,
                                trip_id: rTrip.get('id'),
                                volume: 1,
                                address: rContractorDelivery.get('address'),
                                contractor_delivery_id: rContractorDelivery.get('id'),
                                contractor_delivery_title: rContractorDelivery.get('title_address'),
                                lat: rContractorDelivery.get('lat'),
                                lng: rContractorDelivery.get('lng'),
                                container_type_id: rLastTripPoint.get('container_type_id'),
                                container_status: rTripPoint.get('type') == 'unloading' ? 'full' : 'empty'
                            });
                            rTripPoint.save({
                                success: function () {
                                    grid.store.reload();
                                    winSelectPosition.close();
                                }
                            });
                        });

                        // выбор парковки
                        gridParking.on('celldblclick', function (view, td, cellIndex, rContainerOnParking) {
                            // разгрузка на парковке
                            if (rLastTripPoint && rLastTripPoint.get('type') == 'loading') {
                                rTripPoint.set({
                                    trip_id: rTrip.get('id'),
                                    volume: 1,
                                    parking_id: rContainerOnParking.get('parking_id'),
                                    address: rContainerOnParking.get('parking_address'),
                                    lat: rContainerOnParking.get('parking_lat'),
                                    lng: rContainerOnParking.get('parking_lng'),
                                    container_status: rLastTripPoint.get('container_status'),
                                    container_type_id: rLastTripPoint.get('container_type_id')
                                });
                                rTripPoint.save({
                                    success: function () {
                                        grid.store.reload();
                                        winSelectPosition.close();
                                    }
                                });
                            }
                            // загрузка на парковке
                            if (!rLastTripPoint || rLastTripPoint.get('type') == 'unloading') {
                                if ((cellIndex == 4 && rContainerOnParking.get('count_empty') > 0) || (cellIndex == 6 && rContainerOnParking.get('count_full') > 0)) {
                                    rTripPoint.set({
                                        trip_id: rTrip.get('id'),
                                        volume: 1,
                                        parking_id: rContainerOnParking.get('parking_id'),
                                        address: rContainerOnParking.get('parking_address'),
                                        lat: rContainerOnParking.get('parking_lat'),
                                        lng: rContainerOnParking.get('parking_lng'),
                                        container_status: cellIndex == 4 ? 'empty' : 'full',
                                        container_type_id: rContainerOnParking.get('container_type_id'),
                                        container_type_title: rContainerOnParking.get('container_type_title')
                                    });
                                    rTripPoint.save({
                                        success: function () {
                                            grid.store.reload();
                                            winSelectPosition.close();
                                        }
                                    });
                                }
                            }
                        });

                        // выбор позиции
                        gridPosition.on('itemdblclick', function (view, rPosition) {
                            if (rPosition.get('volume') > 0) {
                                var pType = rPosition.get('planning_type');

                                // под загрузку
                                if (rPosition.get('nomenclature_delivery_type') == 'container' && pType == 'unloading_loading') {
                                    rTripPoint.set({
                                        trip_id: rTrip.get('id'),
                                        client_order_id: rPosition.get('client_order_id'),
                                        client_order_nomenclature_id: rPosition.get('client_order_nomenclature_id'),
                                        client_order_nomenclature_title: rPosition.get('nomenclature_title'),
                                        nomenclature_delivery_type: 'container',
                                        date: null,
                                        volume: 1,
                                        client_order_nomenclature_volume: rPosition.get('client_order_nomenclature_volume'),
                                        unit_title: rPosition.get('unit_title'),
                                        address: rPosition.get('point_from_address'),
                                        contractor_delivery_id: rPosition.get('point_to_contractor_delivery_id'),
                                        contractor_delivery_title: rPosition.get('point_to_title_address'),
                                        lat: rPosition.get('point_to_lat'),
                                        lng: rPosition.get('point_to_lng'),
                                        container_type_id: rPosition.get('container_type_id'),
                                        container_type_title: rPosition.get('container_type_title'),
                                        container_status: 'empty'
                                    });
                                    rTripPoint.save({
                                        success: function () {
                                            grid.store.reload();
                                            winSelectPosition.close();
                                        }
                                    });
                                }

                                // установка, вывоз
                                if (rPosition.get('nomenclature_delivery_type') == 'container' && (pType == 'loading' || pType == 'unloading')) {
                                    rTripPoint.set({
                                        trip_id: rTrip.get('id'),
                                        client_order_id: rPosition.get('client_order_id'),
                                        client_order_nomenclature_id: rPosition.get('client_order_nomenclature_id'),
                                        client_order_nomenclature_title: rPosition.get('nomenclature_title'),
                                        nomenclature_delivery_type: 'container',
                                        date: null,
                                        volume: 1,
                                        client_order_nomenclature_volume: rPosition.get('client_order_nomenclature_volume'),
                                        unit_title: rPosition.get('unit_title'),
                                        address: rPosition.get('point_from_address'),
                                        contractor_delivery_id: rPosition.get('point_to_contractor_delivery_id'),
                                        contractor_delivery_title: rPosition.get('point_to_title_address'),
                                        lat: rPosition.get('point_to_lat'),
                                        lng: rPosition.get('point_to_lng'),
                                        type: rPosition.get('planning_type'),
                                        container_type_id: rPosition.get('container_type_id'),
                                        container_type_title: rPosition.get('container_type_title'),
                                        container_status: rPosition.get('planning_type') == 'loading' ? 'full' : 'empty'
                                    });
                                    rTripPoint.save({
                                        success: function () {
                                            grid.store.reload();
                                            winSelectPosition.close();
                                        }
                                    });
                                }

                                // перевозка контейнера
                                if (rPosition.get('nomenclature_delivery_type') == 'transportation_container') {
                                    rTripPoint.set({
                                        loading_trip_point_id: null,
                                        trip_id: rTrip.get('id'),
                                        client_order_id: rPosition.get('client_order_id'),
                                        client_order_nomenclature_id: rPosition.get('client_order_nomenclature_id'),
                                        client_order_nomenclature_title: rPosition.get('nomenclature_title'),
                                        nomenclature_delivery_type: 'transportation_container',
                                        date: null,
                                        volume: 1,
                                        client_order_nomenclature_volume: rPosition.get('client_order_nomenclature_volume'),
                                        unit_title: rPosition.get('unit_title'),
                                        address: rPosition.get('point_from_address'),
                                        contractor_delivery_id: rPosition.get('point_from_contractor_delivery_id'),
                                        contractor_delivery_title: rPosition.get('point_from_title_address'),
                                        lat: rPosition.get('point_from_lat'),
                                        lng: rPosition.get('point_from_lng'),
                                        container_type_id: rPosition.get('container_type_id'),
                                        container_type_title: rPosition.get('container_type_title'),
                                        container_status: 'full'
                                    });
                                    rTripPoint.save({
                                        success: function () {
                                            grid.store.reload();
                                            winSelectPosition.close();
                                        }
                                    });
                                }
                            }
                        });
                        return;
                    }
                    // добавление точки прицепа
                    if (!rTripPoint.get('has_trailer') && (rTripPoint.get('type') == 'loading' || rTripPoint.get('type') == 'unloading')) {
                        var rSecondTripPoint = Ext.create('app.model.trip.point', {
                            primary_trip_point_id: rTripPoint.get('id'),
                            trip_id: rTripPoint.get('trip_id'),
                            volume: 1,
                            datetime_arrival: rTripPoint.get('datetime_arrival'),
                            datetime_departure: rTripPoint.get('datetime_departure'),
                            type: rTripPoint.get('type')
                        });
                        rSecondTripPoint.save({
                            success: function () {
                                grid.store.reload();
                            }
                        });
                    }
                }
            }
        },
        {
            text: 'Заказ',
            width: 50,
            renderer: function (val, meta, rTripPoint) {
                return rTripPoint.get('is_repair') ? rTripPoint.get('repair_id') : rTripPoint.get('client_order_id');
            }
        },
        {
            text: 'Строка заказа',
            dataIndex: 'client_order_nomenclature_id',
            hidden: true,
            width: 50
        },
        {
            xtype: 'actioncolumn',
            width: 30,
            menuDisabled: true,
            sortable: false,
            align: 'center',
            getClass: function (view, meta, rTripPoint) {
                if (rTripPoint.get('is_repair')) {
                    return (rTripPoint.get('repair_id') && App.Global.checkRights('repair.view')) ? 'icon_bullet_magnify' : false;
                } else {
                    return (rTripPoint.get('client_order_id') && App.Global.checkRights('client_order.view', rTripPoint.get('client_order_manager_id'))) ? 'icon_bullet_magnify' : false;
                }
            },
            handler: function (grid, row) {
                var rTripPoint = grid.getStore().getAt(row);
                if (rTripPoint.get('is_repair') && rTripPoint.get('repair_id') && App.Global.checkRights('repair.view')) {
                    var cRepair = Ext.create('app.controller.garage.repair.repair');
                    cRepair.open(rTripPoint.get('repair_id'));
                }
                if (rTripPoint.get('client_order_id') && App.Global.checkRights('client_order.view', rTripPoint.get('client_order_manager_id'))) {
                    var cClientOrder = Ext.create('app.controller.client_order');
                    cClientOrder.open(rTripPoint.get('client_order_id'));
                }
            }
        },
        {
            text: 'Тип контейнера',
            dataIndex: 'container_status_title',
            flex: 1,
            renderer: function (value, metaData, rec) {
                return rec.get('container_status_fact') && rec.get('container_status_fact') != rec.get('container_status') ? ('<span style="color:#e00">' + rec.get('title') + '</span>') : rec.get('title');
            },
            field: {
                xtype: 'combobox',
                store: Ext.create('Ext.data.ArrayStore', {
                    fields: [
                        {name: 'status', type: 'string'},
                        {name: 'title', type: 'string'}
                    ],
                    data: [
                        ['empty', 'пустой'],
                        ['full', 'полный']
                    ]
                }),
                displayField: 'title',
                editable: false,
                listeners: {
                    select: function (combo, records) {
                        var rec = records[0];
                        var grid = this.up('grid');
                        var rPoint = grid.getSelectionModel().selected.items[0];
                        rPoint.set('container_status_fact', rec.get('status'));
                        rPoint.save({
                            success: function () {
                                grid.store.reload();
                            },
                            failure: function (record, operation) {
                                var errors = operation.request.scope.reader.jsonData["errors"];
                                console.log(errors);
                            }
                        });
                    }
                }
            }
        },
        {
            text: 'Контрагент',
            dataIndex: 'contractor_title',
            flex: 1
        },
        {
            text: 'Номенклатура',
            dataIndex: 'client_order_nomenclature_title',
            flex: 1
        },
        {
            text: 'Адрес',
            dataIndex: 'contractor_delivery_title',
            flex: 2,
            field: {
                xtype: 'triggerfield',
                trigger1Cls: Ext.baseCSSPrefix + 'form-point-trigger',
                onTrigger1Click: function () {
                    var grid = this.up('grid');
                    var rPoint = grid.getSelectionModel().getSelection()[0];

                    // выбор точек парковки
                    if (rPoint.get('type') == 'start' || rPoint.get('type') == 'finish') {
                        var cParking = Ext.create('app.controller.parking');
                        var winParking = cParking.openList();
                        var gridParking = winParking.down('grid');
                        for (var i in gridParking.events.itemdblclick.listeners) {
                            gridParking.removeListener('itemdblclick', gridParking.events.itemdblclick.listeners[i].fn)
                        }
                        gridParking.on('itemdblclick', function (view, rParking) {
                            rPoint.set({
                                parking_id: rParking.get('id'),
                                address: rParking.get('address'),
                                lat: rParking.get('lat'),
                                lng: rParking.get('lng')
                            });
                            rPoint.save();
                            grid.store.reload();
                            winParking.close();
                        });
                    }

                    // смена свалки
                    if (rPoint.get('type') == 'unloading' && rPoint.get('container_status') == 'full') {
                        Ext.syncRequire([
                            'app.view.client_order.supplier_select.win',
                            'app.view.client_order.supplier_select.grid'
                        ]);
                        var rPointLoading = grid.store.getAt(rPoint.index - 1);
                        var winSupplier = Ext.widget('client_order.supplier_select.win');
                        var gridSupplier = winSupplier.down('grid');
                        winSupplier.nomenclature_id = rPoint.get('nomenclature_id');
                        winSupplier.container_type_id = rPoint.get('container_type_id');
                        winSupplier.contractor_delivery = {
                            id: rPoint.get('id'),
                            client_order_id: rPoint.get('client_order_id'),
                            lat: rPointLoading.get('lat'),
                            lng: rPointLoading.get('lng')
                        };
                        winSupplier.show();

                        gridSupplier.on('itemdblclick', function (view, rec) {
                            rPoint.set({
                                contractor_delivery_id: rec.get('contractor_delivery_id'),
                                lat: rec.get('contractor_delivery_lat'),
                                lng: rec.get('contractor_delivery_lng')
                            });
                            rPoint.save({
                                success: function () {
                                    grid.store.reload();
                                    winSupplier.close();
                                }
                            });
                        });
                    }
                }
            }
        },
        {
            text: 'Инв. номер',
            dataIndex: 'container_inventory_number',
            width: 70,
            field: {
                xtype: 'triggerfield',
                editable: false,
                trigger1Cls: Ext.baseCSSPrefix + 'form-point-trigger',
                onTrigger1Click: function () {
                    var grid = this.up('grid');
                    var rPoint = grid.getSelectionModel().getSelection()[0];
                    var cContainer = Ext.create('app.controller.container');
                    var winContainer = cContainer.openListForSelect({
                        trip_point_id: rPoint.get('id'),
                        status: 1
                    });
                    var gridContainer = winContainer.down('grid');
                    gridContainer.on('itemdblclick', function (view, rContainer) {
                        rPoint.set({
                            container_id: rContainer.get('id'),
                            container_inventory_number: rContainer.get('inventory_number')
                        });
                        rPoint.save({
                            success: function () {
                                grid.store.reload();
                                winContainer.close();
                            },
                            failure: function (record, operation) {
                                var errors = operation.request.scope.reader.jsonData["errors"];
                                console.log(errors);
                            }
                        });
                    });
                }
            }
        },
        {
            text: 'Кол-во П',
            dataIndex: 'volume',
            width: 70,
            align: 'right',
            renderer: function (value, metaData, rec) {
                var unit_title = rec.get('unit_title') ? rec.get('unit_title') : 'шт';
                return value ? (value + ' ' + unit_title) : '-';
            },
            field: {
                xtype: 'numberfield',
                minValue: 1,
                maxValue: 99,
                decimalPrecision: 2
            }
        },
        {
            text: 'Кол-во Ф',
            dataIndex: 'volume_fict',
            width: 70,
            align: 'right',
            tdCls: 'background_grey_blue',
            renderer: function (value, metaData, rec) {
                if (rec.get('volume_fict') == 0 && rec.get('volume_fact') == 0) {
                    return '-';
                } else {
                    var unit_title = rec.get('unit_title') ? rec.get('unit_title') : 'шт';
                    value = rec.get('volume_fict') > 0 && rec.get('volume_fict') != rec.get('volume_fact') ? '<span style="color:#e00">' + rec.get('volume_fict') + '</span>' : rec.get('volume_fact');
                    return value + ' ' + unit_title;
                }
            },
            field: {
                xtype: 'numberfield',
                minValue: 1,
                maxValue: 99,
                decimalPrecision: 2
            }
        },
        {
            xtype: 'actioncolumn',
            itemId: 'trip_point_files',
            width: 30,
            align: 'center',
            sortable: false,
            menuDisabled: true,
            getClass: function (view, meta, rTripPoint) {
                return rTripPoint.get('file_cnt') > 0 ? 'icon_camera' : false;
            },
            handler: function (view, row) {
                var grid = this.up('grid');
                var rTripPoint = grid.store.getAt(row);
                if (rTripPoint.get('file_cnt') > 0) {
                    window.open('/trip/point_files/' + rTripPoint.get('id'), '_blank');
                }
            }
        },
        {
            xtype: 'datecolumn',
            text: 'Выполнение',
            dataIndex: 'datetime_arrival_fact',
            width: 120,
            format: 'd.m.Y H:i',
            field: {
                xtype: 'datefield',
                format: 'd.m.Y H:i'
            }
        },
        {
            text: 'простой',
            dataIndex: 'client_order_nomenclature_takeaway_days',
            align: 'right',
            width: 70,
            renderer: function (value) {
                return value ? (value + ' дн') : '';
            }
        },
        {
            text: 'ТТН',
            dataIndex: 'ttn',
            width: 70,
            field: {
                xtype: 'textfield'
            }
        },
        {
            xtype: 'actioncolumn',
            itemId: 'document_closing',
            width: 30,
            align: 'center',
            sortable: false,
            getClass: function (v, meta, rec) {
                if (rec.get('contractor_type') == 'client' && rec.get('document_closing')) {
                    meta.tdAttr = 'data-qtip="Закрывающие документы"';
                    return 'icon_blue_document_exclamation';
                }
                return false;
            },
            handler: function (view, row) {
                var grid = this.up('grid');
                var rec = grid.store.getAt(row); // выбранная точка
                var сTrip = Ext.create('app.controller.trip.main');
                сTrip.getContractorDocumentClosing(rec);
            }
        },
        {
            xtype: 'numbercolumn',
            text: 'Тариф',
            align: 'right',
            format: '0.00',
            width: 70,
            renderer: function (value, metaData, rec) {
                value = rec.get('driver_payment_sum') > 0 ? rec.get('driver_payment_sum') : (rec.get('tariff_bp') ? rec.get('tariff_driver') : rec.get('tariff_driver_ext'));
                return Ext.util.Format.number(value, '0,000.00');
            }
        },
        {
            text: 'Удержание',
            itemId: 'col-driver_fine',
            dataIndex: 'driver_fine_title',
            field: {
                xtype: 'combobox',
                editable: false,
                multiSelect: true,
                store: Ext.create('app.store.dictionary.driver_fine'),
                displayField: 'title',
                listConfig: {
                    getInnerTpl: function () {
                        return '<div class="x-combo-list-item"><img src="' + Ext.BLANK_IMAGE_URL + '" class="chkCombo-default-icon chkCombo" width="16" /> {title}</div>';
                    }
                },
                listeners: {
                    beforequery: function (queryPlan) {
                        queryPlan.combo.store.pageSize = 999;
                        queryPlan.combo.store.proxy.extraParams.status = 1;    // только активные
                    },
                    expand: function () {
                        if (this.value.length) {
                            this.setValue(this.value[0].split(', '));
                        }
                    },
                    beforeselect: function (combo, rDriverFine) {
                        var grid = this.up('grid');
                        var rTripPoint = grid.getSelectionModel().getSelection()[0];
                        Ext.Ajax.request({
                            url: '/trip/driver_fine',
                            method: 'POST',
                            params: {
                                trip_point_id: rTripPoint.get('id'),
                                driver_fine_id: rDriverFine.get('id')
                            }
                        });
                    },
                    beforedeselect: function (combo, rDriverFine) {
                        var grid = this.up('grid');
                        var rTripPoint = grid.getSelectionModel().getSelection()[0];
                        Ext.Ajax.request({
                            url: '/trip/driver_fine',
                            method: 'DELETE',
                            params: {
                                trip_point_id: rTripPoint.get('id'),
                                driver_fine_id: rDriverFine.get('id')
                            }
                        });
                    }
                }
            }
        },
        {
            text: 'Б/П',
            dataIndex: 'tariff_bp',
            width: 30,
            align: 'center',
            renderer: function (val) {
                return val ? '&#10003;' : '';
            },
            field: {
                xtype: 'checkboxfield',
                inputValue: 1
            }
        },
        {
            xtype: 'numbercolumn',
            text: 'Тариф без коэф.',
            dataIndex: 'tariff_driver',
            align: 'right',
            width: 70,
            hidden: true
        },
        {
            xtype: 'actioncolumn',
            itemId: 'delete',
            width: 30,
            align: 'center',
            sortable: false,
            menuDisabled: true,
            getClass: function (v, meta, rTripPoint) {
                var rTrip = this.up('window').down('form').getRecord();
                if (rTrip.get('state') == 'checked' || !App.Global.checkRights('trip.edit_points')) {
                    return false;
                }
                if (rTripPoint.get('is_confirm')) {
                    return 'icon_lock';
                } else {
                    if (rTripPoint.get('state') == 'done') {
                        return 'icon_broom';
                    }
                    if (rTripPoint.get('type') == 'unloading' && rTripPoint.get('trip_id') != rTripPoint.get('loading_trip_point_trip_id') && rTripPoint.get('loading_trip_point_trip_id')) {
                        return 'icon_arrow_rotate_clockwise';
                    }
                    if (rTripPoint.get('type') != 'start' && rTripPoint.get('type') != 'finish' && !rTripPoint.get('loading_trip_point_id')) {
                        return 'icon_delete';
                    }
                }
                return false;
            },
            handler: function (view, row) {
                var self = this;
                var grid = this.up('grid');
                var rTripPoint = grid.store.getAt(row);
                var rTrip = this.up('window').down('form').getRecord();

                // блокировка редактирования
                if (rTripPoint.get('is_confirm') || rTrip.get('state') == 'checked' || !App.Global.checkRights('trip.edit_points')) {
                    return false;
                }

                // очистка данных, пришедших от водителя
                if (rTripPoint.get('state') == 'done') {
                    Ext.Msg.show({
                        msg: 'Вы действительно хотите очистить данные, полученые от водителя?',
                        buttons: Ext.Msg.YESNO,
                        fn: function (btn) {
                            if (btn == 'yes') {
                                rTripPoint.set({
                                    state: 'plan',
                                    datetime_arrival_fact: null,
                                    datetime_departure_fact: null,
                                    container_id: null,
                                    container_inventory_number: null,
                                    volume_fact: null
                                });
                                grid.store.sync();
                            }
                        }
                    });
                    return;
                }

                // возврат точки в ее рейс
                if (rTripPoint.get('type') == 'unloading' && rTripPoint.get('trip_id') != rTripPoint.get('loading_trip_point_trip_id') && rTripPoint.get('loading_trip_point_trip_id')) {
                    Ext.Msg.show({
                        msg: 'Вернуть точку в рейс ' + rTripPoint.get('loading_trip_point_trip_id') + '?',
                        buttons: Ext.Msg.YESNO,
                        fn: function (btn) {
                            if (btn == 'yes') {
                                rTripPoint.set('trip_id', rTripPoint.get('loading_trip_point_trip_id'));
                                rTripPoint.set('n', null);
                                rTripPoint.save({
                                    success: function () {
                                        grid.store.reload();
                                    }
                                });
                            }
                        }
                    });
                    return;
                }

                // удаление точки
                if (rTripPoint.get('type') != 'start' && rTripPoint.get('type') != 'finish' && !rTripPoint.get('loading_trip_point_id')) {
                    Ext.Ajax.request({
                        url: '/trip/point/' + rTripPoint.get('id'),
                        method: 'DELETE',
                        success: function (result) {
                            var data = JSON.parse(result.responseText);
                            if (data.success) {
                                grid.store.reload();
                            }
                        }
                    });
                }
            }
        }
    ],
    initComponent: function () {
        this.store = Ext.create('app.store.trip.point');
        this.callParent(arguments);
    },
    dockedItems: [
        {
            xtype: 'toolbar',
            itemId: 'toolbar_top',
            dock: 'top',
            items: [
                {
                    text: 'Добавить',
                    itemId: 'btn_add',
                    iconCls: 'icon_add',
                    padding: 5,
                    handler: function () {
                        Ext.syncRequire([
                            'app.view.trip.select_position.container.win',
                            'app.view.trip.select_position.container.grid_contractor',
                            'app.view.trip.select_position.container.grid_parking',
                            'app.view.trip.select_position.container.grid_position'
                        ]);

                        var win = this.up('window');
                        var form = win.down('form');
                        var grid = win.query('grid:not([hidden])')[0];
                        var rTrip = form.getRecord();
                        var rLastTripPoint = grid.getLastTripPoint();

                        var winSelectPosition = Ext.widget('trip.select_position.container.win');
                        var gridContractor = winSelectPosition.query('#contractor')[0];
                        var gridParking = winSelectPosition.query('#parking')[0];
                        var gridPosition = winSelectPosition.query('#position')[0];
                        winSelectPosition.show();

                        // свалки
                        gridContractor.store.proxy.extraParams = {
                            supplier_nomenclature_delivery_type: 'container',
                            status: 1
                        };
                        gridContractor.store.pageSize = 999;
                        gridContractor.store.loadPage(1);

                        // парковки
                        gridParking.store.proxy.extraParams = {
                            status: 1
                        };
                        gridParking.store.loadPage(1);

                        // позиции
                        gridPosition.store.proxy.extraParams = {
                            planning_date: Ext.Date.format(rTrip.get('planning_date'), 'Y-m-d'),
                            type: 'container',
                            closed: 1
                        };
                        gridPosition.store.loadPage(1);

                        // выбор свалки
                        gridContractor.on('itemdblclick', function (view, rContractorDelivery) {
                            var rTripPointUnloading = Ext.create('app.model.trip.point', {
                                loading_trip_point_id: null,
                                trip_id: rTrip.get('id'),
                                volume: 1,
                                address: rContractorDelivery.get('address'),
                                contractor_delivery_id: rContractorDelivery.get('id'),
                                contractor_delivery_title: rContractorDelivery.get('title_address'),
                                lat: rContractorDelivery.get('lat'),
                                lng: rContractorDelivery.get('lng'),
                                type: 'unloading',
                                container_type_id: rLastTripPoint.get('container_type_id'),
                                container_status: 'full'
                            });
                            rTripPointUnloading.save({
                                success: function () {
                                    var rTripPointLoading = Ext.create('app.model.trip.point', {
                                        loading_trip_point_id: rTripPointUnloading.get('id'),
                                        trip_id: rTrip.get('id'),
                                        volume: 1,
                                        address: rContractorDelivery.get('address'),
                                        contractor_delivery_id: rContractorDelivery.get('id'),
                                        contractor_delivery_title: rContractorDelivery.get('title_address'),
                                        lat: rContractorDelivery.get('lat'),
                                        lng: rContractorDelivery.get('lng'),
                                        type: 'loading',
                                        container_type_id: rLastTripPoint.get('container_type_id'),
                                        container_status: 'empty'
                                    });
                                    rTripPointLoading.save({
                                        success: function () {
                                            grid.store.reload();
                                            winSelectPosition.close();
                                        },
                                        failure: function (record, operation) {
                                            var errors = operation.request.scope.reader.jsonData["errors"];
                                            console.log(errors);
                                        }
                                    });
                                },
                                failure: function (record, operation) {
                                    var errors = operation.request.scope.reader.jsonData["errors"];
                                    console.log(errors);
                                }
                            });
                        });

                        // выбор парковки
                        gridParking.on('celldblclick', function (view, td, cellIndex, rContainerOnParking) {
                            // разгрузка на парковке
                            if (rLastTripPoint.get('type') == 'loading') {
                                var rTripPointUnloading = Ext.create('app.model.trip.point', {
                                    trip_id: rTrip.get('id'),
                                    volume: 1,
                                    parking_id: rContainerOnParking.get('parking_id'),
                                    address: rContainerOnParking.get('parking_address'),
                                    lat: rContainerOnParking.get('parking_lat'),
                                    lng: rContainerOnParking.get('parking_lng'),
                                    type: 'unloading',
                                    container_status: rLastTripPoint.get('container_status'),
                                    container_type_id: rLastTripPoint.get('container_type_id')
                                });
                                rTripPointUnloading.save({
                                    success: function () {
                                        grid.store.reload();
                                        winSelectPosition.close();
                                    },
                                    failure: function (record, operation) {
                                        var errors = operation.request.scope.reader.jsonData["errors"];
                                        console.log(errors);
                                    }
                                });
                            }

                            // загрузка на парковке
                            if (rLastTripPoint.get('type') == 'start' || rLastTripPoint.get('type') == 'unloading') {
                                if ((cellIndex == 4 && rContainerOnParking.get('count_empty') > 0) || (cellIndex == 6 && rContainerOnParking.get('count_full') > 0)) {
                                    var rTripPointLoading = Ext.create('app.model.trip.point', {
                                        trip_id: rTrip.get('id'),
                                        volume: 1,
                                        parking_id: rContainerOnParking.get('parking_id'),
                                        address: rContainerOnParking.get('parking_address'),
                                        lat: rContainerOnParking.get('parking_lat'),
                                        lng: rContainerOnParking.get('parking_lng'),
                                        type: 'loading',
                                        container_status: cellIndex == 4 ? 'empty' : 'full',
                                        container_type_id: rContainerOnParking.get('container_type_id'),
                                        container_type_title: rContainerOnParking.get('container_type_title')
                                    });
                                    rTripPointLoading.save({
                                        success: function () {
                                            grid.store.reload();
                                            winSelectPosition.close();
                                        },
                                        failure: function (record, operation) {
                                            var errors = operation.request.scope.reader.jsonData["errors"];
                                            console.log(errors);
                                        }
                                    });
                                }
                            }
                        });

                        // выбор позиции
                        gridPosition.on('itemdblclick', function (view, rPosition) {
                            if (rPosition.get('disable_planning')) {
                                Ext.Msg.alert('Внимание!', 'Планирование запрещено! Превышен лимит долга.');
                                return;
                            }
                            if (rPosition.get('volume') > 0) {
                                var pType = rPosition.get('planning_type');

                                // под загрузку
                                if (rPosition.get('nomenclature_delivery_type') == 'container' && pType == 'unloading_loading') {
                                    var rTripPointUnloading = Ext.create('app.model.trip.point', {
                                        trip_id: rTrip.get('id'),
                                        client_order_id: rPosition.get('client_order_id'),
                                        client_order_nomenclature_id: rPosition.get('client_order_nomenclature_id'),
                                        client_order_nomenclature_title: rPosition.get('nomenclature_title'),
                                        nomenclature_delivery_type: 'container',
                                        date: null,
                                        volume: 1,
                                        client_order_nomenclature_volume: rPosition.get('client_order_nomenclature_volume'),
                                        unit_title: rPosition.get('unit_title'),
                                        address: rPosition.get('point_from_address'),
                                        contractor_delivery_id: rPosition.get('point_to_contractor_delivery_id'),
                                        contractor_delivery_title: rPosition.get('point_to_title_address'),
                                        lat: rPosition.get('point_to_lat'),
                                        lng: rPosition.get('point_to_lng'),
                                        type: 'unloading',
                                        container_type_id: rPosition.get('container_type_id'),
                                        container_type_title: rPosition.get('container_type_title'),
                                        container_status: 'empty'
                                    });
                                    rTripPointUnloading.save({
                                        success: function () {
                                            var rTripPointLoading = Ext.create('app.model.trip.point', {
                                                loading_trip_point_id: rTripPointUnloading.get('id'),
                                                trip_id: rTrip.get('id'),
                                                client_order_id: rPosition.get('client_order_id'),
                                                client_order_nomenclature_id: rPosition.get('client_order_nomenclature_id'),
                                                client_order_nomenclature_title: rPosition.get('nomenclature_title'),
                                                nomenclature_delivery_type: 'container',
                                                date: null,
                                                volume: 1,
                                                client_order_nomenclature_volume: rPosition.get('client_order_nomenclature_volume'),
                                                unit_title: rPosition.get('unit_title'),
                                                address: rPosition.get('point_from_address'),
                                                contractor_delivery_id: rPosition.get('point_to_contractor_delivery_id'),
                                                contractor_delivery_title: rPosition.get('point_to_title_address'),
                                                lat: rPosition.get('point_to_lat'),
                                                lng: rPosition.get('point_to_lng'),
                                                type: 'loading',
                                                container_type_id: rPosition.get('container_type_id'),
                                                container_type_title: rPosition.get('container_type_title'),
                                                container_status: 'full'
                                            });
                                            rTripPointLoading.save({
                                                success: function () {
                                                    grid.store.reload();
                                                    winSelectPosition.close();
                                                },
                                                failure: function (record, operation) {
                                                    var errors = operation.request.scope.reader.jsonData["errors"];
                                                    console.log(errors);
                                                }
                                            });
                                        },
                                        failure: function (record, operation) {
                                            var errors = operation.request.scope.reader.jsonData["errors"];
                                            console.log(errors);
                                        }
                                    });
                                }

                                // установка, вывоз
                                if (rPosition.get('nomenclature_delivery_type') == 'container' && (pType == 'loading' || pType == 'unloading')) {
                                    var rTripPointFrom = Ext.create('app.model.trip.point', {
                                        trip_id: rTrip.get('id'),
                                        client_order_id: rPosition.get('client_order_id'),
                                        client_order_nomenclature_id: rPosition.get('client_order_nomenclature_id'),
                                        client_order_nomenclature_title: rPosition.get('nomenclature_title'),
                                        nomenclature_delivery_type: 'container',
                                        date: null,
                                        volume: 1,
                                        client_order_nomenclature_volume: rPosition.get('client_order_nomenclature_volume'),
                                        unit_title: rPosition.get('unit_title'),
                                        address: rPosition.get('point_from_address'),
                                        contractor_delivery_id: rPosition.get('point_to_contractor_delivery_id'),
                                        contractor_delivery_title: rPosition.get('point_to_title_address'),
                                        lat: rPosition.get('point_to_lat'),
                                        lng: rPosition.get('point_to_lng'),
                                        type: rPosition.get('planning_type'),
                                        container_type_id: rPosition.get('container_type_id'),
                                        container_type_title: rPosition.get('container_type_title'),
                                        container_status: rPosition.get('planning_type') == 'loading' ? 'full' : 'empty'
                                    });
                                    rTripPointFrom.save({
                                        success: function () {
                                            grid.store.reload();
                                            winSelectPosition.close();
                                        },
                                        failure: function (record, operation) {
                                            var errors = operation.request.scope.reader.jsonData["errors"];
                                            console.log(errors);
                                        }
                                    });
                                }

                                // перевозка контейнера
                                if (rPosition.get('nomenclature_delivery_type') == 'transportation_container') {
                                    var rTripPointFromFull = Ext.create('app.model.trip.point', {
                                        loading_trip_point_id: null,
                                        trip_id: rTrip.get('id'),
                                        client_order_id: rPosition.get('client_order_id'),
                                        client_order_nomenclature_id: rPosition.get('client_order_nomenclature_id'),
                                        client_order_nomenclature_title: rPosition.get('nomenclature_title'),
                                        nomenclature_delivery_type: 'transportation_container',
                                        date: null,
                                        volume: 1,
                                        client_order_nomenclature_volume: rPosition.get('client_order_nomenclature_volume'),
                                        unit_title: rPosition.get('unit_title'),
                                        address: rPosition.get('point_from_address'),
                                        contractor_delivery_id: rPosition.get('point_from_contractor_delivery_id'),
                                        contractor_delivery_title: rPosition.get('point_from_title_address'),
                                        lat: rPosition.get('point_from_lat'),
                                        lng: rPosition.get('point_from_lng'),
                                        type: 'loading',
                                        container_type_id: rPosition.get('container_type_id'),
                                        container_type_title: rPosition.get('container_type_title'),
                                        container_status: 'full'
                                    });
                                    rTripPointFromFull.save({
                                        success: function () {
                                            var rTripPointToFull = Ext.create('app.model.trip.point', {
                                                loading_trip_point_id: rTripPointFromFull.get('id'),
                                                trip_id: rTrip.get('id'),
                                                client_order_id: rPosition.get('client_order_id'),
                                                client_order_nomenclature_id: rPosition.get('client_order_nomenclature_id'),
                                                client_order_nomenclature_title: rPosition.get('nomenclature_title'),
                                                nomenclature_delivery_type: 'transportation_container',
                                                date: null,
                                                volume: 1,
                                                client_order_nomenclature_volume: rPosition.get('client_order_nomenclature_volume'),
                                                unit_title: rPosition.get('unit_title'),
                                                address: rPosition.get('point_to_address'),
                                                contractor_delivery_id: rPosition.get('point_to_contractor_delivery_id'),
                                                contractor_delivery_title: rPosition.get('point_to_title_address'),
                                                lat: rPosition.get('point_to_lat'),
                                                lng: rPosition.get('point_to_lng'),
                                                type: 'unloading',
                                                container_type_id: rPosition.get('container_type_id'),
                                                container_type_title: rPosition.get('container_type_title'),
                                                container_status: 'full'
                                            });
                                            rTripPointToFull.save({
                                                success: function () {
                                                    var rTripPointFromEmpty = Ext.create('app.model.trip.point', {
                                                        loading_trip_point_id: null,
                                                        trip_id: rTrip.get('id'),
                                                        client_order_id: rPosition.get('client_order_id'),
                                                        client_order_nomenclature_id: rPosition.get('client_order_nomenclature_id'),
                                                        client_order_nomenclature_title: rPosition.get('nomenclature_title'),
                                                        nomenclature_delivery_type: 'transportation_container',
                                                        date: null,
                                                        volume: 1,
                                                        client_order_nomenclature_volume: rPosition.get('client_order_nomenclature_volume'),
                                                        unit_title: rPosition.get('unit_title'),
                                                        address: rPosition.get('point_to_address'),
                                                        contractor_delivery_id: rPosition.get('point_to_contractor_delivery_id'),
                                                        contractor_delivery_title: rPosition.get('point_to_title_address'),
                                                        lat: rPosition.get('point_to_lat'),
                                                        lng: rPosition.get('point_to_lng'),
                                                        type: 'loading',
                                                        container_type_id: rPosition.get('container_type_id'),
                                                        container_type_title: rPosition.get('container_type_title'),
                                                        container_status: 'empty'
                                                    });
                                                    rTripPointFromEmpty.save({
                                                        success: function () {
                                                            var rTripPointToEmpty = Ext.create('app.model.trip.point', {
                                                                loading_trip_point_id: rTripPointFromEmpty.get('id'),
                                                                trip_id: rTrip.get('id'),
                                                                client_order_id: rPosition.get('client_order_id'),
                                                                client_order_nomenclature_id: rPosition.get('client_order_nomenclature_id'),
                                                                client_order_nomenclature_title: rPosition.get('nomenclature_title'),
                                                                nomenclature_delivery_type: 'transportation_container',
                                                                date: null,
                                                                volume: 1,
                                                                client_order_nomenclature_volume: rPosition.get('client_order_nomenclature_volume'),
                                                                unit_title: rPosition.get('unit_title'),
                                                                address: rPosition.get('point_from_address'),
                                                                contractor_delivery_id: rPosition.get('point_from_contractor_delivery_id'),
                                                                contractor_delivery_title: rPosition.get('point_from_title_address'),
                                                                lat: rPosition.get('point_from_lat'),
                                                                lng: rPosition.get('point_from_lng'),
                                                                type: 'unloading',
                                                                container_type_id: rPosition.get('container_type_id'),
                                                                container_type_title: rPosition.get('container_type_title'),
                                                                container_status: 'empty'
                                                            });
                                                            rTripPointToEmpty.save({
                                                                success: function () {
                                                                    grid.store.reload();
                                                                    winSelectPosition.close();
                                                                },
                                                                failure: function (record, operation) {
                                                                    var errors = operation.request.scope.reader.jsonData["errors"];
                                                                    console.log(errors);
                                                                }
                                                            });
                                                        },
                                                        failure: function (record, operation) {
                                                            var errors = operation.request.scope.reader.jsonData["errors"];
                                                            console.log(errors);
                                                        }
                                                    });
                                                },
                                                failure: function (record, operation) {
                                                    var errors = operation.request.scope.reader.jsonData["errors"];
                                                    console.log(errors);
                                                }
                                            });
                                        },
                                        failure: function (record, operation) {
                                            var errors = operation.request.scope.reader.jsonData["errors"];
                                            console.log(errors);
                                        }
                                    });
                                }
                            }
                        });

                    }
                },
                {
                    text: 'Добавить ремонт',
                    iconCls: 'icon_add',
                    padding: 5,
                    handler: function () {
                        var grid = this.up('window').query('grid:not([hidden])')[0];
                        var cTrip = Ext.create('app.controller.trip.main');
                        var rTrip = this.up('window').down('form').getRecord();
                        var winRepair = cTrip.openListRepair(rTrip);
                        var gridRepair = winRepair.down('grid');
                        var rLastTripPoint = grid.getLastTripPoint();

                        gridRepair.on('itemdblclick', function (view, rRepair) {
                            Ext.Ajax.request({
                                url: '/garage/repair/planning/' + rRepair.get('id'),
                                success: function (result) {
                                    var data = JSON.parse(result.responseText);
                                    if (data.success) {
                                        grid.store.reload();
                                        winRepair.close();
                                    } else {
                                        Ext.Msg.alert('', data.errors.join('\n'));
                                    }
                                }
                            });
                        });
                    }
                },
                '->',
                {
                    xtype: 'button',
                    text: 'Удержание',
                    itemId: 'btn-driver_fine',
                    iconCls: 'icon_coins_in_hand',
                    padding: 5,
                    enableToggle: true,
                    handler: function () {
                        var grid = this.up('grid');
                        grid.query('#col-driver_fine')[0].setVisible(this.pressed);
                    }
                },
                {
                    text: 'Внести оплату',
                    itemId: 'btn_payment',
                    iconCls: 'icon_total_plan_cost',
                    padding: 5,
                    handler: function () {
                        var win = this.up('window');
                        var form = win.down('form');
                        var rTrip = form.getRecord();
                        var cTrip = Ext.create('app.controller.trip.main');
                        cTrip.openPayment(rTrip);
                    }
                }
            ]
        }
    ],
    plugins: [
        {
            ptype: 'cellediting',
            clicksToEdit: 1,
            listeners: {
                beforeedit: function (editor, e) {
                    var rTrip = e.grid.up('window').down('form').getRecord();
                    var rPoint = e.record;
                    // права на редактирование точек
                    if (!App.Global.checkRights('trip.edit_points')) {
                        return false;
                    }
                    // блокировка при статусе рейса "Проверено"
                    if (rTrip.get('state') == 'checked') {
                        return false;
                    }
                    // право на редактирвоание удержаний по рейсу
                    if (e.field == 'driver_fine_title' && !App.Global.checkRights('trip.driver_fine.edit')) {
                        return false;
                    }
                    // точка контрагента
                    if (e.field == 'contractor_delivery_title') {
                        if (rPoint.get('type') == 'unloading' && rPoint.get('container_status') == 'full') {
                            return true;
                        }
                    }
                    // инв. номер контейнера
                    if (e.field == 'container_inventory_number') {
                        if (!rPoint.get('container_type_id')) {
                            return false;
                        }
                    }
                    // порядок сортировки
                    if (e.field == 'n') {
                        return rPoint.get('type') == 'loading' || rPoint.get('type') == 'unloading' || rPoint.get('type') == 'start_repair';
                    }
                    // плановый фиктивный объем
                    if (e.field == 'volume') {
                        return rPoint.get('type') != 'start' && rPoint.get('type') != 'finish' && rPoint.get('type') != 'start_repair';
                    }
                    // фиктивный объем
                    if (e.field == 'volume_fict') {
                        return rPoint.get('type') != 'start' && rPoint.get('type') != 'finish' && rPoint.get('type') != 'start_repair';
                    }
                    // адреса для позиций "клиент-постащик" и точки старта и финиша
                    if (e.field == 'contractor_delivery_title') {
                        return (rPoint.get('nomenclature_delivery_type') == 'client_supplier' && rPoint.get('type') == 'unloading')
                            || (rPoint.get('nomenclature_delivery_type') == 'supplier_client' && rPoint.get('type') == 'loading')
                            || rPoint.get('type') == 'start'
                            || rPoint.get('type') == 'finish';
                    }
                    // ТТН только для точек загрузка/разгрузка в точках клиента
                    if (e.field == 'ttn') {
                        return (rPoint.get('type') == 'loading' || rPoint.get('type') == 'unloading') &&
                            rPoint.get('contractor_type') == 'client' &&
                            rPoint.get('is_duty_point');
                    }
                    return true;
                },
                edit: function (editor, e) {
                    var grid = e.grid;
                    grid.store.sync({
                        success: function () {
                            grid.store.reload();
                        },
                        failure: function () {
                            var errors = this.getReader().jsonData["errors"];
                            console.log(errors);
                        }
                    });
                }
            }
        }
    ],
    viewConfig: {
        getRowClass: function (rec) {
            return rec.get('state') == 'done' ? 'active-row' : 'no-active-row';
        }
    },

    /**
     * Получение предыдущей точки
     */
    getLastTripPoint: function (rTripPoint) {
        var pos = rTripPoint ? this.store.indexOf(rTripPoint) : this.store.getCount();
        // нахождение предыдущей точки
        for (var i = pos - 1; i >= 0; i--) {
            // для точек прицепа
            if (rTripPoint && rTripPoint.get('primary_trip_point_id')) {
                if (this.store.getAt(i).get('primary_trip_point_id')) {
                    return this.store.getAt(i);
                }
            } else {
                // для основных точек
                for (var i = pos - 1; i >= 0; i--) {
                    var rec = this.store.getAt(i);
                    if (!rec.get('primary_trip_point_id') && rec.get('type') != 'finish' && !rec.get('repair_id')) {
                        return this.store.getAt(i);
                    }
                }
            }
        }
        return null;
    }
});