Ext.define('app.view.trip.main.grid.removed', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.trip.main.grid.removed',
    initComponent: function () {
        this.store = Ext.create('app.store.trip.point');
        this.callParent(arguments);
    },
    columns: [
        {
            text: 'Код',
            dataIndex: 'id',
            width: 50,
            hidden: true
        },
        {
            xtype: 'actioncolumn',
            width: 20,
            align: 'center',
            sortable: false,
            menuDisabled: true,
            getClass: function (v, meta, rec) {
                return rec.get('iconCls');
            }
        },
        {
            xtype: 'datecolumn',
            format: 'd.m.Y H:i',
            text: 'Дата изменения',
            dataIndex: 'last_delete_date',
            width: 100
        },
        {
            text: 'Тип точки',
            dataIndex: 'type_title',
            width: 70
        },
        {
            xtype: 'actioncolumn',
            width: 30,
            align: 'center',
            menuDisabled: true,
            getClass: function (v, meta, rec) {
                if (rec.get('primary_trip_point_id')) {
                    return 'icon_baggage_cart_box';
                }
            }
        },
        {
            text: 'Заказ',
            width: 50,
            renderer: function (val, meta, rTripPoint) {
                return rTripPoint.get('is_repair') ? rTripPoint.get('repair_id') : rTripPoint.get('client_order_id');
            }
        },
        {
            text: 'Контрагент',
            dataIndex: 'contractor_title',
            flex: 1
        },
        {
            text: 'Номенклатура',
            dataIndex: 'client_order_nomenclature_title',
            flex: 1
        },
        {
            text: 'Кол-во П',
            dataIndex: 'volume',
            width: 70,
            align: 'right',
            renderer: function (value, metaData, rec) {
                var unit_title = rec.get('unit_title') ? rec.get('unit_title') : 'шт';
                return value ? (value + ' ' + unit_title) : '-';
            }
        },
        {
            text: 'Автор',
            dataIndex: 'last_delete_user_name',
            flex: 1
        },
        {
            text: 'Причина',
            dataIndex: 'last_delete_note',
            width: 240
        },
        {
            xtype: 'actioncolumn',
            width: 30,
            align: 'center',
            sortable: false,
            menuDisabled: true,
            getClass: function (v, meta, rTripPoint) {
                var rTrip = this.up('window').down('form').getRecord();
                if (rTrip.get('state') != 'checked' && App.Global.checkRights('trip.edit_points')) {
                    meta.tdAttr = 'data-qtip="восстановить"';
                    return 'icon_arrow_refresh';
                }
            },
            handler: function (view, row) {
                var rTrip = this.up('window').down('form').getRecord();
                if (rTrip.get('state') != 'checked' && App.Global.checkRights('trip.edit_points')) {
                    var grid = this.up('grid');
                    var rTripPoint = grid.store.getAt(row);
                    rTripPoint.set('status', 1);
                    rTripPoint.save({
                        success: function () {
                            grid.store.reload();
                        }
                    });
                }
            }
        }
    ]
});