Ext.define('app.controller.contractor.contract', {
    extend: 'Ext.app.Controller',
    /**
     * Форма
     */
    open: function (id, rContractorContract) {
        Ext.syncRequire([
            'app.view.contractor.contract.main.win',
            'app.view.contractor.contract.main.form',
            'app.view.contractor.contract.main.grid_sub_contract',
            'app.view.contractor.contract.send_history.grid',
            'app.view.contractor.contract.send_history.win'
        ]);

        var self = this;
        var win = Ext.widget('contractor.contract.main.win');
        var form = win.down('form');
        var grid = win.down('grid');
        var contractor = this.contractor;
        win.showBtnState(form.getForm().getValues().state);
        if (id) {
            Ext.ModelManager.getModel('app.model.contractor.contract.main').load(id, {
                success: function (rContractorContract) {
                    // для доп. соглашений другой контроллер
                    if (rContractorContract.get('contract_id')) {
                        var cContractorSubContract = Ext.create('app.controller.contractor.sub_contract', {
                            create_user_name: App.Global.user.name
                        });
                        cContractorSubContract.open(rContractorContract.get('id'));
                        return;
                    }

                    form.loadRecord(rContractorContract);
                    grid.store.proxy.extraParams = {
                        contract_id: rContractorContract.get('id'),
                        status: 1
                    };
                    win.defaultData = form.getValues();
                    win.show();
                    win.rContractorContract = rContractorContract;
                    self.setRules(win);
                    grid.store.loadPage(1);

                    grid.on('itemdblclick', function (view, rContractorSubContract) {
                        var cContractorSubContract = Ext.create('app.controller.contractor.sub_contract');
                        var win = cContractorSubContract.open(rContractorSubContract.get('id'));
                        win.on('close', function () {
                            grid.store.loadPage(1);
                        });
                    });
                }
            });
        } else {
            if (!rContractorContract) {
                rContractorContract = Ext.create('app.model.contractor.contract.main', {
                    contractor_id: contractor.id,
                    contractor_title: contractor.title,
                    create_user_name: App.Global.user.name,
                    type: this.contractor.org_type == 2 ? 2 : 1,
                    is_send_notification: true,
                    status: 1
                });
            }
            form.loadRecord(rContractorContract);
            win.defaultData = form.getValues();
            win.show();
            win.rContractorContract = rContractorContract;
            self.setRules(win);
        }
        // добавление нового доп.соглашения
        var btnAdd = grid.query('#add')[0];
        btnAdd.on('click', function () {
            var rContractorContract = form.getRecord();
            var cContractorSubContract = Ext.create('app.controller.contractor.sub_contract');
            var rContractorSubContract = Ext.create('app.model.contractor.contract.sub_contract', {
                contract_id: rContractorContract.get('id'),
                contract_title: rContractorContract.get('title'),
                contractor_id: rContractorContract.get('contractor_id'),
                contractor_title: rContractorContract.get('contractor_title'),
                action_from_date: rContractorContract.get('action_from_date'),
                action_to_date: rContractorContract.get('action_to_date'),
                create_user_name: App.Global.user.name,
                status: 1
            });
            var win = cContractorSubContract.open(null, rContractorSubContract);
            win.on('close', function () {
                grid.store.loadPage(1);
            });
        });

        // обработка закрытия
        win.confirmClosed = true;
        win.on('beforeclose', function () {

            // если объект не изменился, то проверка не нужна
            var data = this.down('form').getForm().getValues();
            if (JSON.stringify(data) == JSON.stringify(this.defaultData)) {
                win.confirmClosed = false;
            }

            if (win.confirmClosed) {
                Ext.Msg.show({
                    msg: 'Данные были изменены. Сохранить изменения?',
                    buttons: Ext.Msg.YESNOCANCEL,
                    fn: function (btn) {
                        if (btn == 'no') {
                            win.confirmClosed = false;
                            win.close();
                        }
                        if (btn == 'yes') {
                            self.save(win, null, true);
                        }
                        return true;
                    }
                });
                return false;
            } else {
                return true;
            }
        });
    },
    /**
     * Открытие формы с историей отправления счетов
     * @param rContractorContract
     */
    openSendHistory: function (rContractorContract) {
        var win = Ext.widget('contractor.contract.send_history.win');
        var grid = win.down('grid');
        grid.store.proxy.extraParams.contractor_contract_id = rContractorContract.get('id');
        grid.store.loadPage(1);
        win.show();
    },

    /**
     * Форма
     * @param rContractorContract
     */
    openPreview: function (rContractorContract, parentWin) {
        Ext.syncRequire([
            'app.view.contractor.contract.preview.win',
            'app.view.contractor.contract.preview.form'
        ]);
        var win = Ext.widget('contractor.contract.preview.win', {parentWin: parentWin});
        var form = win.down('form').getForm();
        var buttons = win.getDockedComponent('buttons');
        var self = this;
        if (rContractorContract.get('id')) {
            form.loadRecord(rContractorContract);
            Ext.ModelManager.getModel('app.model.contractor.contract.main').load(rContractorContract.get('id'), {
                success: function (rec) {
                    form.loadRecord(rec);
                    form.defaultData = form.getValues();
                    win.setTitle(win.title + " " + rContractorContract.get('doc_num'));
                    win.show();
                }
            });
        } else {
            console.log('не выбран id');
        }
        form.defaultData = form.getValues();
        win.show();
    }
    ,

    /**
     * Сохранение измененных данных
     */
    save: function (win, action, close) {
        win.confirmClosed = false;
        var form = win.down('form');
        var grid = win.down('grid');

        win.query('#btn-save')[0].disable();

        // запись, полученная из формы
        var rContractorContract = form.getRecord();
        var dContractorContract = form.getValues();
        dContractorContract.status = dContractorContract.na == 1 ? 2 : 1;
        dContractorContract.is_frame = dContractorContract.is_frame == 1 ? 1 : 0;
        dContractorContract.prolongation = dContractorContract.prolongation == 1 ? 1 : 0;
        dContractorContract.is_send_notification = dContractorContract.is_send_notification == 1 ? 1 : 0;
        rContractorContract.set(dContractorContract);

        // сохранение объекта
        rContractorContract.save({
            params: {
                action: action
            },
            success: function () {
                form.loadRecord(rContractorContract);
                grid.store.proxy.extraParams = {
                    contract_id: rContractorContract.get('id'),
                    status: 1
                };
                win.defaultData = form.getValues();
                win.query('#btn-save')[0].enable();
                if (close) {
                    win.close();
                }
            },
            failure: function (record, operation) {
                var errors = operation.request.scope.reader.jsonData["errors"];
                form.getForm().markInvalid(errors);
                win.query('#btn-save')[0].enable();
            }
        });
    },
    /**
     * Общий список
     */
    openList: function () {
        Ext.syncRequire([
            'app.view.contractor.contract.list.win',
            'app.view.contractor.contract.list.grid',
            'app.view.contractor.contract.list.state'
        ]);

        var self = this;
        var win = Ext.widget('contractor.contract.list.win');
        var grid = win.down('grid');
        var state = win.query('#state')[0];
        state.store.load();
        grid.store.proxy.extraParams.status = 1;
        grid.store.loadPage(1);
        win.show();
        // обработка двойного клика в гриде
        grid.on('itemdblclick', function (view, rContractorContract) {
            self.open(rContractorContract.get('id'));
        });
        state.on('itemclick', function (view, rContractorContract) {
            grid.store.proxy.extraParams = {
                state: rContractorContract.get('name'),
                state_type: (rContractorContract.get('title') == 'Мои' || rContractorContract.get('state_type') == 'my') ? 'my' : rContractorContract.get('state_type'),
                status: 1
            };
            grid.store.loadPage(1);
        });
    },
    /**
     * Установка прав
     * @param win
     */
    setRules: function (win) {
        var rContractorContract = win.rContractorContract;

        win.confirmClosed = App.Global.checkRights('contractor.contract.edit');
        App.Global.showElement(win.query('#btn-save')[0], App.Global.checkRights('contractor.contract.edit', rContractorContract.get('contractor_manager_id')) || !rContractorContract.get('id')); // редактирование
    },

    /**
     * Открытие формы с историей изменения статуса
     * @param rContractorContract
     */
    openStateHistory: function (rContractorContract) {
        Ext.syncRequire([
            'app.view.contractor.contract.state_history.win',
            'app.view.contractor.contract.state_history.grid'
        ]);
        var win = Ext.widget('contractor.contract.state_history.win');
        var grid = win.down('grid');
        grid.store.proxy.extraParams.contractor_contract_id = rContractorContract.get('id');
        grid.store.loadPage(1);
        win.show();
    }


});
